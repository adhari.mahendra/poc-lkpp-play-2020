/*
 Navicat Premium Data Transfer

 Source Server         : Local Postgres
 Source Server Type    : PostgreSQL
 Source Server Version : 100011
 Source Host           : localhost:5433
 Source Catalog        : poc-2020
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 100011
 File Encoding         : 65001

 Date: 31/01/2020 00:17:24
*/


-- ----------------------------
-- Table structure for barang
-- ----------------------------
DROP TABLE IF EXISTS "barang";
CREATE TABLE "barang" (
  "id" int8 NOT NULL DEFAULT NULL,
  "nama" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "deskripsi" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "harga" float8 DEFAULT NULL,
  "stok" int8 DEFAULT NULL,
  "user_id" int8 DEFAULT NULL
)
;
ALTER TABLE "barang" OWNER TO "postgres";

-- ----------------------------
-- Records of barang
-- ----------------------------
BEGIN;
INSERT INTO "barang" VALUES (1, 'Kerupuk udang', 'udang', 100000, 96, 3);
INSERT INTO "barang" VALUES (2, 'Laptop', 'laptop unyil', 5000000, 10, 3);
INSERT INTO "barang" VALUES (3, 'HP', 'Smartphone', 1000000, 91, 3);
COMMIT;

-- ----------------------------
-- Primary Key structure for table barang
-- ----------------------------
ALTER TABLE "barang" ADD CONSTRAINT "barang_pkey" PRIMARY KEY ("id");
