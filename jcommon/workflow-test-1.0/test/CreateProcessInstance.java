import java.io.FileReader;
import java.io.Reader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import models.workflow.instance.ProcessInstance;
import models.workflow.instance.StateInstance;

import org.h2.tools.Csv;
import org.junit.Test;

import play.Logger;
import play.cache.Cache;
import play.test.FunctionalTest;

public class CreateProcessInstance extends FunctionalTest {
	
	public void beforeTest()
	{
		Cache.add("jcommon.workflow.browsing.enable", true);
	}

	
	public void create() throws Exception {
		runTest("test/data/test-scenario-PASCA_1S_GUGUR.csv");
	}
	
	public void createLelangUlang() throws Exception {
		beforeTest();
		runTest("test/data/test-scenario-evaluasi-ulang-PASCA_1S_GUGUR.csv");
	}
	
	
	public void createLelangPRA_2S_KUA() throws Exception {
		beforeTest();
		runTest("test/data/test-scenario-PRA_2S_KUA_Bagian1.csv");
	}
	
	@Test
	public void createLelangPASCA_1S_GUGUR_N2() throws Exception {
		beforeTest();
		runTest("test/data/test-scenario-PASCA_1S_GUGUR_N2.csv");
	}
	
	
	
	/**Menjalankan test yang didefinisikan di file CSV.
	 * Isi file sbb
	 * 1. terdiri atas 3 kolom
	 * 2. Kolom 1 berisi action: createWorkflow, setVariable, startInstance, testCurrentState, dan gotoNextState
	 * 3. Untuk tiap2 action, kolom-kolom berikutnya berisi
		 * 	a. createWorkflow, deskripsi, workflow-id
		 * 	b. setVariable, VARIABLE, VALUE
		 *  c. gotoNextState
		 *  d. startInstance
		 *  e. testCurrentState, "TAHAP1, TAHAP2, TAHAP3, DST"
	 *  
	 *  4. Komentar diawali tanda #
	 *  5. Baris kosong dapat diisikan dan akan diabaikan
	 *  6. Variabel dengan tipe Date harus dalam format yyyy-MM-dd HH:mm
	 *  */
	
	public static void runTest(String csvFile) throws Exception
	{
		Csv csv=new Csv();
		String[] columns=new String[] {"action","variable","value"};
		Reader reader=new FileReader(csvFile);
		ResultSet rs=csv.read(reader, columns);
		
		//create process instance
		ProcessInstance pi=null;
		while(rs.next())
		{
			String action=rs.getString(1);
			if(action!=null)
				if(action.startsWith("#"))
					Logger.debug("Action: %s", action);
				else
				{
					//create a workflow
					if("createWorkflow".equals(action))
					{
						pi=ProcessInstance.create(rs.getString("VALUE"), rs.getString(2));
						Logger.debug("########### Create process instance %s ############", pi.process_instance_id);
					}
					else
					if("setVariable".equals(action))
						setVariable(pi, rs);
					else
					if("startInstance".equals(action))
						pi.startInstance();					
					else
					if("gotoNextState".equals(action))
						gotoNextState(pi);
					else
					if("testCurrentState".equals(action))
						testCurrentState(pi, rs);
					else
						throw new Exception("Action tidak dikenali: " + action);
				}
		}
		reader.close();
	}

	private static void testCurrentState(ProcessInstance pi, ResultSet rs) throws SQLException {
		String testProcessInstanceList=rs.getString(2) + ",";
		List<StateInstance>list=pi.getActiveStateInstanceList();
		//cek apakah semua ada di list
		int matchCount=0;
		for(StateInstance si: list)
		{
			String stateName=si.getState().name + ",";
			if(!testProcessInstanceList.contains(stateName))
				break;
			matchCount++;
		}
		//Current state yg di CSV tidak sesuai dengan yg ada di database
		assertFalse("StateInstance di CSV tidak sesuai dengan di database. Di File:\n" + testProcessInstanceList + ", di DB:\n" + pi.getCurrentStateInstanceString() , matchCount != list.size());
	}

	private static void gotoNextState(ProcessInstance pi) throws Exception {		
		pi.gotoNextState();
	}

	private static void setVariable(ProcessInstance pi, ResultSet rs) throws SQLException, Exception {
		String variable=rs.getString("VARIABLE");
		String value=rs.getString("VALUE");
		pi.setVariable(variable, value);		
	}
	

}