package models.dce.client;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.dce.db.Exporter;
import models.jcommon.db.base.JdbcUtil;
import models.jcommon.util.TempFileManager;

import org.h2.tools.Csv;

import play.Logger;

import com.google.gson.Gson;

/**Exporter di sisi LPSE */
public class ClientExporter extends Exporter{

	
	public ClientExporter(String sessionId, String query, String primaryKey) throws SQLException {
		super(sessionId, query, primaryKey);
		
	}
	
	protected void init()
	{
		temporaryTableNameMD5 ="T_MD5_CLIENT_" + Math.abs(sessionId.hashCode());
		//must call super.init()
		super.init();
	}

	public void execute() throws SQLException {
		//generate MD5 Table
		super.exportMD5Info();
	}

	/**Merge dengan data MD5 dari server.
	 * Data dari server disimpan dalam inputStream sebagai CSV lalu dibaca oleh CSVReader
	 * 
	 * Data client ada di H2 database
	 * @param is data MD5 dari DCE Server
	 * @throws IOException 
	 * @throws SQLException */
	public File mergeMD5DataFromServer(InputStream is) throws IOException, SQLException {
		Csv csvReader=new Csv();
		LineNumberReader reader=new LineNumberReader(new InputStreamReader(is));
		ResultSet rsServer=csvReader.read(reader, null);
		Connection conn=dataSourceH2.getConnection();
		
		//buat QUery untuk membaca data dari client/H2
		StringBuilder sqlClientSelect =new StringBuilder();
		StringBuilder whereSql=new StringBuilder();
		sqlClientSelect.append("SELECT * FROM ");
		sqlClientSelect.append(temporaryTableNameMD5);
		
		int col=0;
		for(String pk:manipulator.primaryKeys) //primarykeys mungkin kosong
		{
			if(col>0)
				whereSql.append(" AND ");
			if(col==0)
				whereSql.append(" where ");
			whereSql.append(pk).append("=?");
			col++;
		}
		
		sqlClientSelect.append(whereSql);
		String sqlActionRow="UPDATE " + temporaryTableNameMD5 + " SET " + MD5_COLUMN_NAME + "=? " +
				whereSql.toString();
		PreparedStatement stClient=conn.prepareStatement(sqlClientSelect.toString());
		PreparedStatement stActionRow=conn.prepareStatement(sqlActionRow);
				
		//rsServer berisi data yang dikirim oleh DCE/Server
		String actionRow=null;
		int skipCount=0;
		int insertCount=0;
		int updateCount=0;
		
		List<List<String>> listDeletedRows=new ArrayList<List<String>>(); 
		while(rsServer.next())
		{
			/* baca setiap row dari server(CSV) dan bandingkan dengan di client (H2).
			 * 1. jika di client ADA, MD5 sama, maka SKIP
			 * 2. jika di client ADA, MD5 beda, maka UPDATE 
			 * 3. jika di client ADA, di server tidak ada maka INSERT
 			 * 4. jika di client TDK-ADA, maka DELETE
			 */
			
			//pkValues digunakan untuk debugging dan generate SQL 'INSERT INTO(...)
			List<String> pkValues=processPK(stClient, stActionRow, rsServer);
			
			
			ResultSet rsClient=stClient.executeQuery();
			if(rsClient.next())
			{
				String md5Server=rsServer.getString(MD5_COLUMN_NAME);
				String md5Client=rsClient.getString(MD5_COLUMN_NAME);
				if(md5Server.equals(md5Client))			
				{
					//kondisi 1
					actionRow=null;
					skipCount++;
				}
				else
				{
					//kondisi 2
					actionRow=ACTION_UPDATE;
					updateCount++;
				}
			}
			else
				//4: Delete
				listDeletedRows.add(pkValues);
			rsClient.close();
			
			//update kolom MD5 sesuai dengan actionRow
			stActionRow.setString(1, actionRow);
			stActionRow.executeUpdate();
		}
		rsServer.close();
		stClient.close();
		stActionRow.close();

	/* Kondisi 3: INSERT didapat dengan cara menghapus data yang actionRow=null
	 *  lalu set menjadi @INSERT jika kolom MD5_COLUMN_NAME masih berisi MD5
	 */
		Statement st=conn.createStatement();
		st.executeUpdate("DELETE FROM " + temporaryTableNameMD5 + " WHERE " + MD5_COLUMN_NAME + " is null");
		st.close();
				
		PreparedStatement stUpdate=conn.prepareStatement("UPDATE " + temporaryTableNameMD5 + " SET " + MD5_COLUMN_NAME + "=? " +
				"WHERE " + MD5_COLUMN_NAME + " <> ?");
		stUpdate.setString(1, ACTION_INSERT);
		stUpdate.setString(2, ACTION_UPDATE);
		insertCount=stUpdate.executeUpdate();
		
		//nah sekarang kita harus tambahkan row yang DELETE
		if(listDeletedRows.size() > 0)
			insertRowActionDeleteIntoCSV(conn, listDeletedRows);			
				
		
		/* Di titik ini, h2 database berisi data yang siap dikirim ke LPSE, 
		 * kolom MD5_ALL_COLUMNS sekarang berisi string
		 * @INSERT
		 * @UPDATE
		 * @DELETE
		 * 		
		 */

		File temporaryFileMD5=TempFileManager.createFileInTemporaryFolder(sessionId + "FINAL.RESULT.CSV");

		Statement stExport=conn.createStatement();
		 
		ResultSet rs=stExport.executeQuery("SELECT * FROM " + temporaryTableNameMD5);
		FileWriter writer=new FileWriter(temporaryFileMD5);
		Csv csv=new Csv();
		csv.write(writer, rs);
		csv.close();
	
		Logger.debug("FINAL RESULT[%s] of file is in %s" , manipulator.table, temporaryFileMD5);
		Logger.debug("MERGE MD5 DATA[%s] Summary. Insert %s, Update %s, Delete %s, Skip %s", manipulator.table, 
				insertCount, updateCount, listDeletedRows.size(), skipCount);
		
		stUpdate.close();
		stExport.close();
		conn.close();
		is.close();
		
		
		return temporaryFileMD5;
	}
	
	/**Masukkan ke dalam CSV, row-row yang harus didelete oleh DCE
	 * 
	 * @param conn
	 * @param listDeletedRows
	 * @throws SQLException
	 */
	private void insertRowActionDeleteIntoCSV(Connection conn, List<List<String>> listDeletedRows) throws SQLException {
		StringBuilder sql=new StringBuilder();
		sql.append("INSERT INTO ").append(temporaryTableNameMD5);
		sql.append("(").append(MD5_COLUMN_NAME);
		sql.append(',').append(manipulator.primaryKeyString);
		sql.append(") VALUES('").append(ACTION_DELETE).append('\'');
		for(int i=0;i<manipulator.primaryKeys.length;i++)
			sql.append(",?");
		sql.append(')');
		
		PreparedStatement st=conn.prepareStatement(sql.toString());
		for(List<String> listPK: listDeletedRows)
		{
			int index=1;
			for(String pk: listPK)
				st.setString(index++, pk);
			st.executeUpdate();
		}
		st.close();
	}

	/**Memproses primaryKey dari sebuah row 
	 * 
	 * @param stClient
	 * @param stActionRow
	 * @param rsServer
	 * @return List dari PK: pk1, pk2, pk3,...
	 * @throws SQLException
	 */
	private List<String> processPK(PreparedStatement stClient, PreparedStatement stActionRow, ResultSet rsServer) throws SQLException
	{
		List<String> list=new ArrayList<String>();
		  
		int index=1;
		for(String pk: manipulator.primaryKeys)
		{
			String pkValue=rsServer.getString(pk);
			stClient.setString(index++, pkValue);				
			stActionRow.setString(index, pkValue);
			list.add(pkValue);
		}
		return list;
	}
	

	@Override
	protected String getH2DatabaseName() {
		return TempFileManager.baseFolder + "/h2/dce-client";
	}

	/** Untuk Client/LPSE generate semua kolom */
	public boolean isIncludeAllColumns() {
		return true;
	}

	public File exportAggregate(String sql) throws SQLException, IOException {
		//sisikan 1 kolom baru di posisi pertama untuk ACTION
		sql=sql.toLowerCase();
		String sqlWithNewColumn=sql.replaceFirst("select ", "select null as " + MD5_COLUMN_NAME + ", ");
		Logger.debug("Execute Aggregate Function/Sub Query: %s", sqlWithNewColumn);
		ResultSet rs= JdbcUtil.executeQueryUsingJDBC(sqlWithNewColumn);
		super.writeResultsetAsCSV(rs);
		rs.close();
		return temporaryFileMD5;
	}

}
