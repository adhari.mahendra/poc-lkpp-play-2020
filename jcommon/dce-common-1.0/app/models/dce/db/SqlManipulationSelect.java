package models.dce.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.builder.ToStringBuilder;

import play.Logger;

import com.google.gson.Gson;

/**SQL element merupakan hasil parsing dari SQL yang dapat disusun atas
 * 1. command: SELECT
 * 2. fields: String[]
 * 3. table: nama table
 * 4. where: where & order by condition 
 * @author Mr. Andik
 *
 */
public class SqlManipulationSelect {

	private static String PATTERN_AGGREGATE="select.*(sum|avg|count|max|min) *\\(.*";
	
	public String sql;
	public String[] fields;
	public String table;
	private String where;
	public String[] primaryKeys;
	public String primaryKeyString; //primary key dalam format pk1, pk2 .... (tanpa quote dan ']')
	
	public boolean isAggregate;
	
	public boolean containsSubQuery=false;
	
	private String[] fieldsCoal;
	
	/**Lakukan manipulasi terhadap SQL
	 * 
	 * @param SQL yang akan dimanipulasi, berupa SELECT statement
	 * @param primaryKeyColumnsParam daftar primary key dalam format Json
	 * @throws SQLException 
	 */
	public SqlManipulationSelect(String sql, String primaryKeyColumnsParam) throws SQLException
	{
		this(sql, false, primaryKeyColumnsParam);
	}
	
	
	public SqlManipulationSelect(String sql, boolean containsSubQuery,
			String primaryKeyColumnsParam) throws SQLException{
		String str="(select )(.+)( from )(\\w+)(.*)";
		Matcher m=java.util.regex.Pattern.compile(str, Pattern.CASE_INSENSITIVE + Pattern.MULTILINE).matcher(sql);
		isAggregate=Pattern.compile(PATTERN_AGGREGATE, Pattern.CASE_INSENSITIVE + Pattern.MULTILINE).matcher(sql).matches();
		if(m.matches())
		{
			String fieldStr=m.group(2).trim();
			
			/* jika agregate: 
			 * 1. select count(*) as counts from counter
			 * 2. select count(*) as counts, sum(kolom 1) as kolom_1 from counter
			 * 
			 */

			if(isAggregate)
			{
				fields=fieldStr.split(",");
				try
				{
					for(int i=0;i<fields.length;i++)
						fields[i]=fields[i].replaceAll(".* as ", "");
				}
				catch(java.lang.ArrayIndexOutOfBoundsException e)
				{
					Logger.error("java.lang.ArrayIndexOutOfBoundsException: %s",  fieldStr);
				}
			}
			else
				fields=fieldStr.split(",");
			table=m.group(4);
			where=m.group(5);
			/*MD5 tidak dihitung terhadap row-row yang SYSTEM_GENERATED
			 * 
			 */
			if(where.length()==0)
				where=" where audituser <>'#SYSTEM_GENERATED#'";
			else
				where=where+ " AND audituser<>'#SYSTEM_GENERATED#'";
		}
		else
			throw new SQLException("Invalid SELECT ... statement: " + sql);
		boolean error=false;
		if(primaryKeyColumnsParam==null)
			error=true;
		else
			if(primaryKeyColumnsParam.contains("["))
				this.primaryKeys=new Gson().fromJson(primaryKeyColumnsParam,String[].class);
			else
			//mungkin bukan dalam bentuk gson, tapi comma separated 
				this.primaryKeys=primaryKeyColumnsParam.split(",");
		if(error || this.primaryKeys==null)
				throw new IllegalArgumentException("PrimaryKey cannot be empty. SQL: " + sql);
		//buang kolom PK dengan nama lpse_id
		List<String> list=new ArrayList<String>();
		for(String fieldName: primaryKeys)
			if(!fieldName.equalsIgnoreCase("lpse_id"))
				list.add(fieldName);		
		this.primaryKeys=list.toArray(new String[0]);
		primaryKeyColumnsParam=Arrays.toString(this.primaryKeys);
	
		primaryKeyString=primaryKeyColumnsParam.replaceAll("\\[|\\]|\"", "");
		this.sql=sql + " order by " + primaryKeyString;
	}

	public String toString()
	{
		
		return " fields: " + Arrays.toString(fields) + " table: " + table + " where: " + where ;
	}
	
	/**Tambahkan fungsi MD5 pada setiap field
	 * 
	 * @return
	 */
	private void addMD5FunctionToFields()
	{
		/*kolom auditupdate tidak perlu dimasukkan dalam perhitungan MD5. Alasan:
		 * 1. Di database SPSE versi tertentu, nilai_evaluasi.auditupdate  bertipe DATE, harusnya datetime sehingga menghasilkan MD5 yang salah
		 * 2. Auditupdate akan berubah jika ada perubahan di kolom-kolom lainnya  
		 */
		
		int len=fields.length;
		fieldsCoal=new String[len];
		for(int i=0;i<len;i++)
			if(fields[i].equals("auditupdate"))
				fieldsCoal[i]=" '' ";
			else
				fieldsCoal[i]=" COALESCE(md5(''|| " + fields[i]  + "),'')";
	}
	
	/**Dapatkan string yang berisi SELECT MD5(col1) || MD5(col2).... AS MD5_ALL_COLUMNS , pk1, pk2 FROM 
	 * 
	 * @param includeAllColumns jika true, maka kolom-kolom disertakan jika di samping MD5_ALL_COLUMN
	 * 	ini digunakan di sisi client (LPSE)
	 */
	public String getSelectStringMD5(boolean includeAllColumns)
	{
		addMD5FunctionToFields();
		StringBuilder strBuilderAll=new StringBuilder();
		
		StringBuilder strBuilderColumns=new StringBuilder();
		
		strBuilderAll.append("SELECT MD5(");
		int i=0;
		for(String field: fields)
		{
			if(i>0)
			{
				strBuilderAll.append(" || ");
				if(includeAllColumns)
					strBuilderColumns.append(", ");
			}
			strBuilderAll.append(fieldsCoal[i]);
			if(includeAllColumns)
				strBuilderColumns.append(field);
			i++;
		}
		i=0;
		strBuilderAll.append(") as MD5_ALL_COLUMNS ");
		//add primary key or add all other columns
		if(includeAllColumns)
		{
			strBuilderAll.append(", ");
			strBuilderAll.append(strBuilderColumns);
		}
		else
			for(String field: primaryKeys)
			{
				strBuilderAll.append(", ");
				strBuilderAll.append(field);
			}

		strBuilderAll.append(" FROM ");
		strBuilderAll.append(table);
		strBuilderAll.append(where);
		if(primaryKeyString.length() > 0) //add order by primary key if necessary
		{
			strBuilderAll.append(" ORDER BY ");
			strBuilderAll.append(primaryKeyString);
		}
		return strBuilderAll.toString();
	}
	
	
}
