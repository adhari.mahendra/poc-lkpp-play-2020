package jobs;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.lang.time.StopWatch;

import models.jcommon.config.Configuration;
import models.jcommon.mail.MailQueue;
import play.Logger;
import play.Play;
import play.Play.Mode;
import play.db.jpa.NoTransaction;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

/**Job ini akan create MailSenderMultiJob dengan jumlah tertentu (default=5).
 * Jumlah job ini bisa diatur di application.conf -> mail.job.count
 * @author Andik Yulianto<andikyulianto@yahoo.com>
 */
@Deprecated //diganti jobs.BulkSendMailJob
//@OnApplicationStart(async=true) 
//@NoTransaction
public class MailSenderLauncherJob extends Job {

	//untuk memastikan bahwa hanya ada 1 launcher yg running
	private static AtomicBoolean isRunning=new AtomicBoolean(false);
	
	public void doJob()
	{
		if("true".equals(System.getProperty("mail.sending.disabled")) && Play.mode==Mode.DEV)
			return;
		if(isRunning.getAndSet(true))
			return;
		Integer jobCount=Integer.parseInt(Play.configuration.getProperty("mail.job.count", "5"));
		CountDownLatch latch=new CountDownLatch(jobCount);
		Queue<MailQueue> queue=new LinkedList<>();
		/**Urutan yg akan dikirimi
		 * 1. prioritas 0,1,2
		 * 2. retry 0 ---> MAX
		 * 3. lalu ID baru ---> lama
		 * WHERE status INBOX, OUTBOX, RETRY
		 */
		List<MailQueue> list = MailQueue.find(" status IN (0,2,3) ORDER BY prioritas ASC, retry ASC, id DESC").fetch();
		queue.addAll(list);
		StopWatch sw=new StopWatch();
		sw.start();
		
//		Logger.debug("[MailSenderLauncherJob] starts, mailCount: %,d, jobCount: %s" , list.size(), jobCount);
		
		for(int i=0; i<jobCount; i++)
			new MailSenderMultiJob(latch, queue, i).in(i+1);

		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sw.stop();
//		Logger.debug("[MailSenderLauncherJob] all jobs DONE, duration: %s", sw);
		String delayDuration=Configuration.getConfigurationValue(MailQueue.SMTP_DELAY, "60");
		new MailSenderLauncherJob().in(delayDuration + "s");
	}
	
	public void after()
	{
		isRunning.set(false);
	}
	
	public synchronized static void createJobIfNecessary()
	{
		new MailSenderLauncherJob().in(1);
	}
}