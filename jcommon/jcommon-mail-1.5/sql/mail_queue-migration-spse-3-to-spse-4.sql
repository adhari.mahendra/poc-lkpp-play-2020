--Script migration from SPSE 3.0
--new column
--1. column_1, column_2, column_3 digunakan untuk info-info tambahan sekaligus rename dari lls_id (column_1), rkn_id (column_2)
alter table mail_queue add column engine_version varchar(10);
alter table mail_queue add column column_1 numeric(19,0);
alter table mail_queue add column column_2 numeric(19,0);
alter table mail_queue add column column_3 numeric(19,0);

--
/*
2. copy value dari lls_id dan rkn_id
3. Default value for engine_version.
	Data yg lama diset engine_version=3.0.0
*/
update mail_queue set column_1=lls_id, column_2=rkn_id, engine_version='3.0.0';

--4. Delete old column
alter table mail_queue drop column lls_id;
alter table mail_queue drop column  rkn_id;

--5. create index by status
create index idx_mail_queue_status on mail_queue(status);



