package com.iaodt.epns.dce2.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
  
public class DceSecurityV2 {  
    private static Cipher ecipher;  
    private static Cipher dcipher;  
    private static final String mykey="Y95YrM1551CpRb1w";
    private static Logger logger = Logger.getLogger(DceSecurityV2.class);
    static{
    
        try {  
        	SecretKey key = new SecretKeySpec(mykey.getBytes(), "AES");
            ecipher = Cipher.getInstance("AES");  
            dcipher = Cipher.getInstance("AES");  
            ecipher.init(Cipher.ENCRYPT_MODE, key);  
            dcipher.init(Cipher.DECRYPT_MODE, key);  
        } catch (Exception e) {
        	e.printStackTrace();
        	logger.error("Failed in initialization : "+e.getMessage());
        }  
    }  
  
    public static String encrypt(byte[] str) {  
        try {  
            //byte[] utf8 = str.getBytes("UTF-8");  
            byte[] enc = ecipher.doFinal(str);  
              
            return Base64.encodeBase64String(enc); 
        } catch (Exception e) {  
        	logger.error("Failed in Encryption : "+e.getMessage());         
        }  
        return null;  
    }  
  
    public static byte[] decrypt(String str) {  
        try {  
            byte[] dec = Base64.decodeBase64(str);
  
            byte[] utf8 = dcipher.doFinal(dec);  
  
            return utf8;  
        } catch (Exception e) {  
            logger.error("Failed in Decryption : "+e.getMessage());  
        }  
        return null;  
    }  
    
    public static byte[] compressToByte(final String data, final String encoding)
	throws IOException
	{
		if (data == null || data.length() == 0)
		{
			return null;
		}
		else
		{
			byte[] bytes = data.getBytes(encoding);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			GZIPOutputStream os = new GZIPOutputStream(baos);
			os.write(bytes, 0, bytes.length);
			os.close();
			byte[] result = baos.toByteArray();
			return result;
		}
	}

	public static String unCompressString(final byte[] data, final String encoding)
	throws IOException
	{
		if (data == null || data.length == 0)
		{
			return null;
		}
		else
		{
			ByteArrayInputStream bais = new ByteArrayInputStream(data);
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			GZIPInputStream is = new GZIPInputStream(bais);
			byte[] tmp = new byte[data.length];
			while (true)
			{
				int r = is.read(tmp);
				if (r < 0)
				{
					break;
				}
				buffer.write(tmp, 0, r);
			}
			is.close();

			byte[] content = buffer.toByteArray();
			return new String(content, 0, content.length, encoding);
		}
	}
}  
