package models.workflow.definition;

import java.util.List;
import java.util.Set;

import javax.persistence.Transient;
import javax.script.ScriptException;

import models.workflow.WorkflowBaseModel;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/** Satu state mewakili satu kondisi */
@Table(name="WF_STATE")
public class State  extends WorkflowBaseModel{
	
	@Id
	public String state_id;
	
	@Required
	public Integer process_definition_id; // relasi ke processDefinition
	
	@Required
	public String name;
	
	public String description;
	
	public boolean fork;
	
	protected void prePersist()
	{
		super.prePersist();
		state_id=process_definition_id + "." + name;
	}
	

	/**Dapatkan StateMapper list yang dapat dicapai dari StateSaat ini  
	 * @throws ScriptException */
	public List<StateMapper> getNextStateMapperList() throws ScriptException {
		//dapatkan nextState dari state saat ini
		return StateMapper.findNextPossibleStateMapperList(state_id);		
	}
	
	/**Start state jika nama="START" */
	public boolean isStartState()
	{
		return name.equals("START");
	}
	
	/**Start state jika nama="START" */
	public boolean isEndState()
	{
		return name.equals("END");
	}

	/**Pergi ke state berikutnya. Method/field berakhir V hanya untuk validasi
	 NOTE:ALGORITMA masih belum tuntas 
	 * */
	
	public static Set<String> stateVisited;
	
	public boolean testReachToEnd(StringBuilder strResult) {		
		if(stateVisited.contains(state_id))
			return true;
		stateVisited.add(state_id);
		List<StateMapper> list=StateMapper.findNextPossibleStateMapperList(state_id);
		boolean allchildReachToEnd=list.size() > 0;
		if(list!=null)
		{
			for(StateMapper stateMapper:list) {
				if(stateMapper.next_state_id!=null)
				{
					State nextState=stateMapper.getNextState();
					if(!stateVisited.contains(nextState.state_id))
						if(!nextState.testReachToEnd(strResult))
						{
							allchildReachToEnd=false;
							break;
						}
				}		
			}
		}
		//jika ada child yang gak nyampai ke END, masukkan state ini ke result
		if(!allchildReachToEnd)
		{
			if(strResult.length() >0)
				strResult.append(", ");
			strResult.append(this.name);
			
		};
		Logger.debug("Visiting: %s, allchildReachToEnd: %s", name, allchildReachToEnd);
		if(isEndState())
			return true;
		return 
			allchildReachToEnd;
	}
		
	public static State getStartState(Integer processDefinitionId)
	{
		return findByStateNameAndProcessDefinitionId("START", processDefinitionId);
	}

	public static State findByStateNameAndProcessDefinitionId(String stateName, Integer processDefinitionId) {
		return find("name=? and process_definition_id=?", stateName, processDefinitionId).first();
	}
}
