package models.workflow.definition;

import java.util.Collection;
import java.util.List;

import javax.persistence.Transient;
import javax.script.ScriptException;

import models.workflow.WorkflowBaseModel;
import models.workflow.instance.ProcessInstance;
import models.workflow.instance.ScriptRunner;
import models.workflow.instance.VariableInstance;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Id;
import play.db.jdbc.Table;


@Table(name="WF_STATE_MAPPER")
public class StateMapper  extends WorkflowBaseModel{

	@Id(sequence="SEQ_WF_STATE_MAPPER")	
	public Integer state_mapper_id;
	
	public Integer process_definition_id;
	
	@Required
	public String state_id;
	
	@Required
	public String next_state_id;
	
	public String condition;
	
	public String postVisitScript;
	
	@Transient
	public State getCurrentState() {
		return State.findById(state_id);
	}
	
	@Transient
	public State getNextState() {
		return State.findById(next_state_id);
	}
	
	/**Cek apakah diijinkan berpindah ke State berikutnya?
	 * Cek nilai-nilai variable untuk menentukan boleh/tidaknya
	 * 
	 * Untuk testing Script online bisa lihat di http://writecodeonline.com/javascript/
	 * @param variableInstanceList
	 * 
	 * @throws ScriptException */
	public boolean allowGotoNext(ProcessInstance processInstance, Collection<VariableInstance> variableInstanceList, int depth) throws ScriptException {
		if(condition==null)
			return true;
		StringBuilder padding=new StringBuilder();
		String variables=getValueOfSelectedVariables(processInstance);
		for(int i=1;i<depth;i++)
			padding.append("   ");
		try {
			//Jalankan script pada condition
			Object obj=ScriptRunner.runScript(processInstance, variableInstanceList, condition);
			// Handling WHITESPACE
			Boolean result=Boolean.valueOf(obj.toString().trim());
			
			//dapatkan nilai-nilai variabel yang dipakai pada conditions										
//			if(result)
//				Logger.debug("%s Evaluating in [%s] = %s. MOVE STATE. Variables[%s]", padding.toString(), toString(),result,variables);
			return result;
		}
		catch (Exception e) {
			e.printStackTrace();
			Logger.error(e.toString());
			throw new ScriptException("StateMapper: " + toString()  +  "\n" + e.toString() + "\nVariables: " + variables);
		}		
	}
	
	/**Dapatkan nilai dari variable-variable yang ada pada condition */
	private String getValueOfSelectedVariables(ProcessInstance pi) {
		StringBuilder variables=new StringBuilder();
		String[] variableConditions=condition.split("\\W+");
		if(variableConditions!=null)
			for(String str: variableConditions)
			{
				VariableInstance vi=pi.getVariableInstance(str);
				if(vi!=null)
				{
					if(variables.length() > 0)
						variables.append(",");						
					variables.append(str);
					variables.append("=");
					variables.append(vi.value);
				}
			}
		return variables.toString();
	}

	
	public String toString()
	{
		if(next_state_id==null)
			return state_mapper_id + "@" + getCurrentState().name + "(Cond:" + condition +")";
		else
		return state_mapper_id + "@" + getCurrentState().name + "-->" + getNextState().name + "(Cond:" + condition +")";
	}
	
	public static List<StateMapper> findByProcessDefinition(Integer processDefinitionId) {
		return find("process_definition_id=? order by state_mapper_id ", processDefinitionId).fetch();
	}

	/**Dapatkan list dari semua next state yang mungkin */
	public static List<StateMapper> findNextPossibleStateMapperList(String stateId) {
		if(stateId == null || stateId.isEmpty())
			return null;
		return find("state_id=?", stateId).fetch();
	}	
}
