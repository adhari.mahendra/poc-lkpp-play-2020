package models.workflow.definition;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EnumType;
import javax.persistence.Transient;

import models.workflow.WorkflowBaseModel;
import models.workflow.instance.ProcessInstance;
import models.workflow.instance.StateInstance;

import org.h2.tools.Csv;

import play.Logger;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import com.google.gson.Gson;


@Table(name="WF_PROCESS_DEFINITION")
public class ProcessDefinition extends WorkflowBaseModel {
	
	@Enumerated(EnumType.ORDINAL)
	public enum STATUS
	{
		DRAF, //
		AKTIF,
		INAKTIF
	}
	
	@Id(sequence="SEQ_WF_PROCESS_DEFINITION")
	public Integer process_definition_id; 
		
	public String workflow_id;

	public Integer version;	
	
	public STATUS status;
	
	@Transient
	public Set<Variable> variableList;
	
	@Transient
	public List<StateMapper> stateMapperList;	

	
	public Set<Variable> getVariableList() {
		if(variableList == null)
			variableList = new HashSet<Variable>(mapVariable.values());
		return variableList;
	}

	
	public List<StateMapper> getStateMapperList() {
		if(stateMapperList == null)
			stateMapperList = StateMapper.findByProcessDefinition(process_definition_id);
		return stateMapperList;
	}


	public State getStartState()
	{
		State state=State.getStartState(process_definition_id);
		return state;
	}
	
	public State findState(String stateName) {
		return State.findByStateNameAndProcessDefinitionId(stateName, process_definition_id);
	}
	
	
	@Override
	protected void preDelete() {
		StateMapper.delete("process_definition_id = "+process_definition_id);
		State.delete("process_definition_id = "+process_definition_id);
	}

	public void addVariables(Set<String> variables) {
		Variable var = null;
		for(String str: variables)
		{
			var=new Variable();
			var.name=str;
			var.required=false;//sementara tidak digunakan
			mapVariable.put(str, var);
		}		
	}
	
	/**Pastikan bahwa ProcessDefinition ini valid.
	 * Yaitu: 
	 * 1. Dari START, semua state dilalui dan berakhir di END*/
	public String validateProcess()
	{
		State startState=getStartState();
		StringBuilder strResult=new StringBuilder();
		State.stateVisited = new HashSet<String>();
		startState.testReachToEnd(strResult);
		Logger.info("visited state %s", State.stateVisited);
		State.stateVisited.clear();
		if(strResult.length()==0)
			return null;
		else
		return strResult.toString();
	}
	
	public boolean isAktif()
	{
		return STATUS.AKTIF.equals(status);
	}
	
	public Variable getVariable(String variableName)
	{
		variableName=variableName.trim();
		Variable var=mapVariable.get(variableName);		
		return var;
	}
	
	@Transient
	public Map<String, Variable> mapVariable = new HashMap<String, Variable>();
	
	//map di atas disimpan sebagai json
	public String jsonVariable;
	
	@Override
	protected void postLoad()
	{
		if(jsonVariable!=null)
		{
			Type typeToken=new com.google.gson.reflect.TypeToken<Map<String, Variable>>(){}.getType();
			mapVariable=new Gson().fromJson(jsonVariable, typeToken);
		}	
		
	}
	
	protected void prePersist()
	{
		jsonVariable=new Gson().toJson(mapVariable);
		super.prePersist();
	}
 

	/**Menjalankan test yang didefinisikan di file CSV.
	 * Isi file sbb
	 * 1. terdiri atas 3 kolom
	 * 2. Kolom 1 berisi action: createWorkflow, setVariable, startInstance, testCurrentState, dan gotoNextState
	 * 3. Untuk tiap2 action, kolom-kolom berikutnya berisi
		 * 	a. createWorkflow, deskripsi, workflow-id
		 * 	b. setVariable, VARIABLE, VALUE
		 *  c. gotoNextState
		 *  d. startInstance
		 *  e. testCurrentState, "TAHAP1, TAHAP2, TAHAP3, DST"
	 *  
	 *  4. Komentar diawali tanda #
	 *  5. Baris kosong dapat diisikan dan akan diabaikan
	 *  6. Variabel dengan tipe Date harus dalam format yyyy-MM-dd HH:mm
	 *  */
	
	public void testProcessDefinition(Integer processDefinitionId, InputStream csvFile) throws Exception
	{
		Csv csv=new Csv();
		String[] columns=new String[] {"action","variable","value"};
		Reader reader=new InputStreamReader(csvFile);
		ResultSet rs=csv.read(reader, columns);
		ProcessDefinition definisi = ProcessDefinition.findById(processDefinitionId);
		//create process instance
		ProcessInstance pi=null;
		while(rs.next())
		{
			String action=rs.getString(1);
			if(action!=null)
				if(action.startsWith("#"))
					Logger.info("Action: %s", action);
				else
				{
					//create a workflow
					if("createWorkflow".equals(action))
					{
						pi=ProcessInstance.createByProcessDefinition(definisi, "Testing");
						Logger.info("########### Create process instance %s ############", pi.process_instance_id);
					}
					else
					if("setVariable".equals(action))
						setVariable(pi, rs);
					else
					if("startInstance".equals(action))
						pi.startInstance();					
					else
					if("gotoNextState".equals(action))
						gotoNextState(pi);
					else
					if("testCurrentState".equals(action))
						testCurrentState(pi, rs);
					else
						throw new Exception("Action tidak dikenali: " + action);
				}
		}
		reader.close();
	}

	private void testCurrentState(ProcessInstance pi, ResultSet rs) throws Exception {
		String testProcessInstanceList=rs.getString(2) + ",";
		List<StateInstance>list=pi.getActiveStateInstanceList();
		//cek apakah semua ada di list
		int matchCount=0;
		for(StateInstance si: list)
		{
			String stateName=si.getState().name + ",";
			if(!testProcessInstanceList.contains(stateName))
				break;
			matchCount++;
		}
		
		if(matchCount != list.size())
			throw new Exception("StateInstance di CSV tidak sesuai dengan di database. Di File:\n" + testProcessInstanceList + ", di DB:\n" + pi.getCurrentStateInstanceString());
	}

	private void gotoNextState(ProcessInstance pi) throws Exception {		
		pi.gotoNextState();
	}

	private void setVariable(ProcessInstance pi, ResultSet rs) throws SQLException, Exception {
		String variable=rs.getString("VARIABLE");
		String value=rs.getString("VALUE");
		pi.setVariable(variable, value);		
	}
	
	public static ProcessDefinition findByWorkflowIdAndVersion(String workflow, int versi)
	{
		return findByWorkflowIdAndVersionAndStatus(workflow, versi, STATUS.AKTIF);
	}
	
	public static ProcessDefinition findByWorkflowIdAndVersionAndStatus(String workflow, int versi, STATUS status)
	{
		return find("workflow_id=? and version=? and status=?", workflow, versi, status).first();
	}
	
	/**Dapatkan processDefinition dari workflowId tertentu dengan versi terakhir dan status=AKTIF
	 *  */
	public static ProcessDefinition findByWorkflowId(String workflowId)
	{
		return find("workflow_id=? and status=? order by version desc", workflowId, STATUS.AKTIF).first();
	}
	
}
