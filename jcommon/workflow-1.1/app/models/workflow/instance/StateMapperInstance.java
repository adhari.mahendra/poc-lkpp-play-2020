package models.workflow.instance;

import java.util.Date;

import models.workflow.WorkflowBaseModel;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/** Ini untuk menyimpan ketika sebuah state mapper dikunjungi */

@Table(name="WF_STATE_MAPPER_INSTANCE")
public class StateMapperInstance extends WorkflowBaseModel {
	
	@Id(sequence="SEQ_WF_STATE_MAPPER_INSTANCE")
	public Integer state_mapper_instance_id;
	
	public Integer state_mapper_id;

	public boolean visited;
	
	public Date visit_date;
}
