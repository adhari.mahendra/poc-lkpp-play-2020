package models.workflow.instance;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import javax.script.ScriptException;

import com.google.gson.Gson;

import models.workflow.WorkflowBaseModel;
import models.workflow.definition.ProcessDefinition;
import models.workflow.definition.State;
import models.workflow.definition.StateMapper;
import models.workflow.definition.Variable;
import play.Logger;
import play.data.validation.Required;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

/**ProcessInstance merupakan instance dari sebuah ProcessDefinition.
 * 
 * @author andik
 *
 */
@Table(name="WF_PROCESS_INSTANCE")
public class ProcessInstance extends WorkflowBaseModel {
	
	@Enumerated(EnumType.ORDINAL)
	public enum PROCESS_STATUS
	{
		CREATED, RUNNING, COMPLETED, TERMINATED;
		
		public boolean isRunning() {
			return this == RUNNING;
		}
		
		public boolean isCompleted() {
			return this == COMPLETED;
		}
		
		public static PROCESS_STATUS findById(int status) {
			PROCESS_STATUS prosesStatus = null;
			switch (status) {
				case 1:	prosesStatus = CREATED; break;
				case 2: prosesStatus = RUNNING; break;
				case 3: prosesStatus = COMPLETED; break;
				case 4: prosesStatus = TERMINATED; break;
			}
			return prosesStatus;
		}
	}
	
	@Id(sequence="SEQ_WF_PROCESS_INSTANCE")
	public Long process_instance_id;
	
	public Integer process_definition_id;
	
	@Required
	public Date start_date;
//		
	public String description; 	
		
	/**Status dari processInstance
	 * Pertama kalai dibuat statusnya CREATED,
	 * Setelah dipangging startInstance berubah menjadi RUNNING
	 * */
	public PROCESS_STATUS status;
	
	public ProcessDefinition getProcessDefinition() {
		return ProcessDefinition.findById(process_definition_id);
	}

	
	@Override
	protected void preDelete() {
		StateInstance.delete("process_instance_id="+process_instance_id);
		ProcessInstanceLog.delete("process_instance_id="+process_instance_id);
	}
	
	
	/**Start this instance 
	 * @throws Exception */
	public void startInstance() throws Exception
	{
		status=PROCESS_STATUS.RUNNING;		
		State state=getProcessDefinition().getStartState();
		StateInstance stateInstance= StateInstance.findAndCreateByProcessInstanceAndState(this, state);
		stateInstance.process_instance_id = process_instance_id;
		stateInstance.createdate = new Date();
		stateInstance.state_id=state.state_id;
		if(stateInstance.enter_date == null)
			stateInstance.enter_date=new Date();
		stateInstance.active_state=true;
		visitedStateInstanceListLoopDetector = new HashSet<StateMapper>();
		if(stateInstance.gotoNextState())
		{
			stateInstance.active_state=false;
			stateInstance.exit_date=new Date();
		}
		stateInstance.save();
        Logger.info("@startInstance : state_id %s, enter_date %s, exit_date %s", stateInstance.state_id, stateInstance.enter_date, stateInstance.exit_date);
		save();
	}

	/**Goto next state berdasarkan kondisi & variable saat ini.
	 * ProcessInstance harus dalam state Runnning (call terlebih dahulu startInstance())
     * JAngan Gunakan method ini untuk proses workflow lelang di SPSE
	 * @throws ScriptException */
 	public void gotoNextState() throws Exception, IllegalStateException {
		if(status.isCompleted())
			return;
		if(!status.isRunning())
			throw new IllegalStateException("gotoNextState() only applied where status=RUNNING. Status is now: " + status.toString());

		//untuk mendeteksi adanya forever loop pada state.
		//variabel ini menyimpan semua state instance yang sedang di-visit
		//jika di-visit 2 kali artinya looping
		visitedStateInstanceListLoopDetector = new HashSet<StateMapper>();

		
		//dapatkan stateInstance yg aktif		
		List<StateInstance> list=getActiveStateInstanceList();
		for(StateInstance stateInstance: list)
			stateInstance.gotoNextState();
		save();
	}
	
	/**Get current state instance List */
	public List<StateInstance> getActiveStateInstanceList()
	{
//		selalu load kembali karena harus Uptodate
		return StateInstance.findActiveByProcessInstanceId(process_instance_id);
	}
	
	public List<StateInstance> getStateInstanceList()
	{
		return StateInstance.findByProcessInstanceId(process_instance_id);
	}
	
	public String getCurrentStateInstanceString()
	{
		List<StateInstance> list=getActiveStateInstanceList();
		StringBuilder str=new StringBuilder();
		for(StateInstance si: list)
		{
			if(str.length() > 0)
				str.append(", ");
			si.refresh();
			str.append(si.getState().name);
		}
		return str.toString();
	}

	/** Update nilai dari variable-variable
	 * Tidak semua yg ada di Map variables terdapat di processInstance
	 * Map terdiri atas variable dgn nama VARIABLE_DT.xxxx: informasi Date dan VARIABLE_TM.XXXX informasi Time
	 * Selanjutnya VARIABLE_DT. dan VARIABLE_TM di-concat 
	 * 
	 * @param variables key untuk variable dimulai dengan VARIABLE_DT.
	 * @throws Exception 
	 */
	public void updateVariablesValue(Map<String, String[]> variables) throws Exception {
		for(String key:variables.keySet()) {
			if(key.startsWith("VAR_")) {
				String value = variables.get(key)[0];
				String varName = key.substring(4);
				if(value.length()==1)
					value=null;
				VariableInstance variableInstance=getAndCreateVariableInstance(varName, value);
				mapVariableInstance.put(varName, variableInstance);
			}
		}			
		//goto next state
		if(!status.isRunning())
			startInstance();	
		gotoNextState();		
	}
	
	public VariableInstance getVariableInstance(String variableName)
	{
		return mapVariableInstance.get(variableName);
	}
	
	/**Dapatkanv variable instance dari variableName ini.
	 * jika tidak ada maka create baru. 
	 * @param variableName yang diawali TGL diperlakukan sebagai tanggal dengan format yy-MM-yyyy 	 
	 *  jika diikuti informasi waktu maka dalam format HH:mm
	 *  Jika tidak diikuti informasi waktu maka 
	 *  a. jika berakhiran END maka akan ditambahkan 23:59
	 *  b. jika tidak berakhiran END akan ditambahkan 00:00 
	 * @param value */
	public VariableInstance getAndCreateVariableInstance(String variableName, String value) throws Exception
	{
		variableName=variableName.trim();
		VariableInstance var=getVariableInstance(variableName);
		if(var == null)
		{
			var=new VariableInstance(variableName);
			Variable variable=getProcessDefinition().getVariable(variableName);
			if(variable==null)
				throw new Exception("You set value for undefined variable: " + variableName);
		}		
		var.value=value;	
		return var;
	}

	/**Set variable untuk process instance ini 
	 * @throws Exception */
	public void setVariable(String variableName, String value) throws Exception {
		VariableInstance vi=getAndCreateVariableInstance(variableName, value);
		mapVariableInstance.put(variableName, vi);
	}
		
	/**Cek apakah state instance saat ini berisi statename tertentu ? */
	public boolean isCurrentStateInstanceContain(String stateName)
	{
		List<StateInstance> list=getActiveStateInstanceList();
		if(list==null)
			return false;
		for(StateInstance si: list)
		{
			if(si.getState().name.equals(stateName))
				return true;
		}
		return false;
	}
	
	/**Dapatkan informasi saat ini tentang process instance ini */
	public String getInformation()
	{
		StringBuilder str=new StringBuilder();
		str.append("Current State: ");
		List<StateInstance> list=getActiveStateInstanceList();
		int stateCount=0;
		for(StateInstance si: list)
		{
			if(stateCount++ > 0)
				str.append(", ");			
			str.append(si.getState().name);
		}
		return str.toString();
	}
	
	/** Digunakan untuk mendeteksi forever loop di StateInstance.
	 * Harus disimpan pada class processIstance karena inisiasi ada di sini. */
	@Transient
	public Set<StateMapper> visitedStateInstanceListLoopDetector;

	/**Lakukan reset terhadap variable tertentu.  
	 * @throws Exception 
	 * @script merupakan expresi sederhana:
	 *  VARIABLE=value; VARIABLE2=value2;*/
	public void executePostVisitScript(String script) throws Exception {		
		if(script!=null) {
			Collection<VariableInstance> variableInstanceList = mapVariableInstance.values();
			ScriptRunner.runScriptUpdateVariable(this, variableInstanceList, script);
		}
	}
	
	
	/**Add log to this state instance */
	public void addLog(String logInfo)
	{
		ProcessInstanceLog log=new ProcessInstanceLog();
		log.createdate=log.log_date = new Date();
		log.process_instance_id = process_instance_id;
		log.log_info=logInfo;
		log.save();
	}		
	

	
	/**Create sebuah process instance. Cari berdasarkan workflow ID dengan processDefinition yg statusnya 
	 * 
	 * @throws Exception */
	public static ProcessInstance create(String workflowId, String description) throws Exception
	{
		ProcessDefinition pd= ProcessDefinition.findByWorkflowId(workflowId);
		if(pd==null)
			throw new Exception("WorkflowID not found: " + workflowId);
		//create process instance
		ProcessInstance pi=new ProcessInstance();
		pi.process_definition_id=pd.process_definition_id;
		pi.description=description;
		pi.status=PROCESS_STATUS.CREATED;
		pi.start_date=new Date();
		pi.createdate = new Date();
		pi.save();
		Logger.debug("Create process instance Id:%s and workflowId:%s ", pi.process_instance_id, workflowId);
		return pi;
	}
	
	//Create process instance by process Definition Id
	//digunakan untuk testing
	public static ProcessInstance createByProcessDefinition(ProcessDefinition processDefinition, String description) throws Exception
	{		
		//create process instance
		ProcessInstance pi=new ProcessInstance();
		pi.process_definition_id=processDefinition.process_definition_id;
		pi.description=description;
		pi.status=PROCESS_STATUS.CREATED;
		pi.start_date=new Date();
		pi.createdate = new Date();
		pi.save();
		Logger.info("Create process instance Id:%s and workflowId:%s ", pi.process_instance_id, processDefinition.workflow_id);
		return pi;
	}

	/** Dapatakan procesInstance */
	public static ProcessInstance getInstance(Long processInstanceId) {
		return findById(processInstanceId);
	}
	
	public static List<ProcessInstance> findByStatus(PROCESS_STATUS status) {
		return find("status=?", status).fetch();
	}

	public static List<ProcessInstance> findByDescription(String description) {
		return find("description like '%"+description+"%' order by process_instance_id desc").fetch();
	}
		
	/*********************** Coding baru untuk workflow-1.1
	 * 
	 */
	@Transient
	public Map<String, VariableInstance> mapVariableInstance = new HashMap<String, VariableInstance>();
	
	//map di atas disimpan sebagai json
	public String jsonVariableIntance;
	
	
	protected void postLoad()
	{
		super.postLoad();
		if(jsonVariableIntance!=null)
		{
			Type typeToken=new com.google.gson.reflect.TypeToken<Map<String, VariableInstance>>(){}.getType();
			mapVariableInstance=new Gson().fromJson(jsonVariableIntance, typeToken);
		}		
	}
	
	
	protected void prePersist()
	{
		super.prePersist();
		jsonVariableIntance=new Gson().toJson(mapVariableInstance);
	}
}
