package models.workflow;

import java.util.Date;
import java.util.List;

import models.workflow.instance.ProcessInstance.PROCESS_STATUS;
import play.db.jdbc.Query;

public class Process_instance_detail {
	
	public Long process_instance_id;
	
	public Integer process_definition_id;
	
	public String workflow_id;
	
	public PROCESS_STATUS status;
	
	public String description;
	
	public Date start_date;

	public String current_state;
	
	public static List<Process_instance_detail> findAll() {
		String sql = "SELECT process_instance_id, p.process_definition_id, workflow_id,p.status,description,start_date, "
				+ "(SELECT array_to_string(array_agg(s.name), ',') FROM wf_state_instance si, wf_state s WHERE si.state_id=s.state_id AND si.process_instance_id=p.process_instance_id and active_state=true) current_state "
				+ "FROM wf_process_instance p, wf_process_definition d  WHERE p.process_definition_id=d.process_definition_id ORDER BY process_instance_id DESC";
		return Query.find(sql, Process_instance_detail.class).fetch();
	}

}
