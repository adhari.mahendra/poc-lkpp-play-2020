package jobs;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import models.jcommon.config.Configuration;
import models.jcommon.mail.MailQueue;
import models.jcommon.mail.MailQueue.MAIL_PRIORITAS;
import models.jcommon.mail.MailQueue.MAIL_STATUS;
import models.jcommon.util.DateUtil;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.time.StopWatch;

import play.Logger;
import play.Play;
import play.Play.Mode;
import play.cache.Cache;
import play.db.jdbc.Query;
import play.jobs.Job;
import play.jobs.OnApplicationStart;


/**This MailQueue should be safe for cluster. 
 * Each instance may send mail. If one mail has been processed by instance #1,
 * other instances should not send it again
 * 
 * A job to send email queues. If a new email has been added to queue, this Job
 * is triggered immediately.
 * if CONFIG->test.mail is set, all email will be sent here.
 * Properties dari SMTP didapat dari tabel CONFIGURATION('CONFIG','mail.*) yaitu:
 * "mail.delay"
 * "mail.retry"
 * "mail.sender"
 * "mail.smtp.host"
 * "mail.smtp.password"
 * "mail.smtp.user"
 * 
 * */

//TODO IMPORTANT =====> MailJob ini akan running dalam waktu lama, padahal dia menggunakan transaction
/*Untuk itu, harus menggunakan mekanisme tanpa JPA
 * 
 * @author Mr. Andik
 *
 */
@OnApplicationStart(async=true)
public class MailSenderJob extends Job<Object> {
	
	private Properties mailProperties;	
	private String testMail;
	private Session session; // mail session
	private String mailSender; // mail sender LPSE: bisa berupa "Andik Yulianto"<andik@lkpp.go.id>
	private String userSmtp;//beda dengan mail sender, tidak boleh ada Nama Orang. COntoh benar: andik@lkpp.go.id
	private int retryCount;
	private int delayDuration; // in minutes
	private String password;
	private static boolean firstRun=true;
	private static int passCount=0;
	public MailSenderJob() {		
	}
	
	/**Dijalankan hanya satu kali
	 * 
	 */
	private void setup() {
		//simpan di Cache
		mailProperties = Cache.get("mailProperties", Properties.class);
		if(mailProperties==null)
		{
			mailProperties=new Properties();
			mailProperties.put("mail.smtp.host", Configuration.getConfigurationValue(MailQueue.SMTP_HOST,""));
			mailProperties.put("mail.smtp.port", Configuration.getConfigurationValue(MailQueue.SMTP_PORT,""));
			testMail=Configuration.getConfigurationValue(MailQueue.SMTP_MAIL_TEST,"");
		    mailProperties.put("mail.debug", "false");
		    mailProperties.put("mail.smtp.auth", "true");
		    mailProperties.put("mail.transport.protocol", "smtp");
		    mailProperties.put("mail.smtp.starttls.enable", "true");
		    mailProperties.put("mail.smtp.ssl.trust", Configuration.getConfigurationValue(MailQueue.SMTP_HOST, ""));
		    mailSender = Configuration.getConfigurationValue(MailQueue.SMTP_SENDER,"");
		    mailProperties.put("mailSender", mailSender);
		    //dapatkan smtpUser untuk kasus: "Andik Yulianto"<andik@lkpp.go.id>
		    Pattern p=Pattern.compile("\".+\"<(.+)>");
		    Matcher m=p.matcher(mailSender);
		    if(!m.matches())
		    	userSmtp=mailSender;
		    else
		    	userSmtp=m.group(1);
		    mailProperties.put("userSmtp", userSmtp);
		    mailProperties.put("password", Configuration.getConfigurationValue(MailQueue.SMTP_SENDER_PASWORD, ""));
		    mailProperties.put("retryCount", Configuration.getConfigurationValue(MailQueue.SMTP_RETRY, "3"));
		    mailProperties.put("delayDuration", Configuration.getConfigurationValue(MailQueue.SMTP_DELAY, "10"));
		    Cache.set("mailProperties", mailProperties, "1h");
		}
	
		retryCount = Integer.parseInt(mailProperties.getProperty("retryCount"));
		delayDuration = Integer.parseInt(mailProperties.getProperty("delayDuration"));
		password=mailProperties.getProperty("password");
		userSmtp=mailProperties.getProperty("userSmtp");
		mailSender=mailProperties.getProperty("mailSender");
		
		session = Session.getInstance(mailProperties, new Authenticator() {
			 protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(userSmtp, password);
	          }
		});
		
		if(firstRun) //hanya jalan sekali
			try {
				session.getTransport().connect();
				Logger.info("SMTP Server status is OK");
			} catch (MessagingException e) {
				Logger.error("[%s] Error connecting to SMTP Server: %s", Configuration.getConfigurationValue(MailQueue.SMTP_HOST), e);
			}
	}


	@Override
	public void doJob() {
	
		/* In Development environtment, mail sending can be disabled by adding in application launcher
		 * -Dmail.sending.disabled=true
		 */
		if("true".equals(System.getProperty("mail.sending.disabled")) && Play.mode==Mode.DEV)
		{
			Logger.info("[MAIL-JOB DISABLED] MailSenderJob  all mails are not sent ");
			return;
		}
		setup();
		if(firstRun)
		{
			Logger.info("[MAIL-JOB] started. Retry: %s times, Delay: %s minutes", retryCount, delayDuration );
			if(Play.mode==Mode.DEV)
				Logger.info("[MAIL-JOB] This job can be disabled in DEV mode by adding '-Dmail.sending.disabled=true' in Application Launcher", retryCount, delayDuration );
		}
		firstRun=false;
		try {
			passCount++;
			StopWatch sw=new StopWatch();
			sw.start();
			session.getTransport();
			int processedCount=sendSinglePass();
			sw.stop();
			if(processedCount>0)
				Logger.debug("MailSenderJob#%s, Done, processed email: %,d, duration: %s", passCount, processedCount, sw);
		}
		catch(Exception e)
		{
			Logger.error(e, "Error connecting to SMTP %s");
		}
		finally {
		}
		//jalankan job pada sekian menit berikutnya
		new MailSenderJob().in(delayDuration + "min");
	}

	/**
	 * Melakukan satu putaran pengiriman (iterasi ke seluruh tabel MAIL_QUEUE dan dicek
	 * mana yang masih harus dikirim. 
	 * @return false jika tidak ada lagi data
	 * @throws SQLException 
	 */
	private int sendSinglePass() {
		MailQueue mq = null;
		long current_mail_id = Long.MIN_VALUE;
		int processedCount=0;
		
		while(true)
		{
			mq = MailQueue.getNextQueue(current_mail_id);
			if(mq!=null)
			Logger.debug("next email :"+ mq.body);
			if (mq == null) 
				break;
			processedCount++;
//			Logger.debug("[pass: %s index: %s id: %s] Sending email: %s, to: %s, retry: %s", passCount, mailIndex, mq.id, mq.subject, mq.to_addresses, mq.retry);
			//SELALU GUNAKAN TANGGAL RIIL (bukan simulasi)
			mq.send_date=new Date(); // meskipun gagal dikirim, send_date tetap diisi
			mq.audituser="MAIL-JOB";
	
			current_mail_id = mq.id;
	
			/**Proses pengiriman email memerlukan waktu cukup lama (bbrp seconds).
			 * Oleh karena itu, state dari Model harus cepat-cepat di-flush ke database
			 * 
			 */
			mq.status = MAIL_STATUS.SENDING;
			save(mq);
			if(!sendEmail(mq))
				break;
		}
		return processedCount;
	}

	public boolean sendEmail(MailQueue mq) {
		setup();
		
		// SELALU GUNAKAN TANGGAL RIIL (bukan simulasi)
		mq.send_date = DateUtil.newDate(); // meskipun gagal dikirim, send_date tetap diisi
		mq.audituser = "MAIL-JOB";
		Logger.info("sending mail : %s", mq.toString());
		boolean success;
		try {
			// create the messge.
			MimeMessage mimeMessage = new MimeMessage(session);

			mimeMessage.setFrom(new InternetAddress(userSmtp));

			MimeMultipart rootMixedMultipart = new MimeMultipart("mixed");
			mimeMessage.setContent(rootMixedMultipart);

			MimeMultipart nestedRelatedMultipart = new MimeMultipart("related");
			MimeBodyPart relatedBodyPart = new MimeBodyPart();
			relatedBodyPart.setContent(nestedRelatedMultipart);
			rootMixedMultipart.addBodyPart(relatedBodyPart);

			MimeMultipart messageBody = new MimeMultipart("alternative");
			MimeBodyPart bodyPart = null;
			for (int i = 0; i < nestedRelatedMultipart.getCount(); i++) {
				BodyPart bp = nestedRelatedMultipart.getBodyPart(i);
				if (bp.getFileName() == null) {
					bodyPart = (MimeBodyPart) bp;
				}
			}
			if (bodyPart == null) {
				MimeBodyPart mimeBodyPart = new MimeBodyPart();
				nestedRelatedMultipart.addBodyPart(mimeBodyPart);
				bodyPart = mimeBodyPart;
			}
			bodyPart.setContent(messageBody, "text/alternative");

			// Create the HTML text part of the message.
			MimeBodyPart htmlTextPart = new MimeBodyPart();
			String mqBody = new String(Base64.decodeBase64(Base64.decodeBase64(mq.body)));// two step encoding
			htmlTextPart.setContent(mqBody, "text/html;charset=UTF-8");
			messageBody.addBodyPart(htmlTextPart);

			mimeMessage.setFrom(new InternetAddress(mq.from_address));
			String recipient = mq.to_addresses;
			// simulation testmail maybe more than 1
			if (testMail != null && Play.mode.equals(Play.Mode.DEV)) { 
				String[] ary = testMail.split(";");
				for (String to : ary)
					mimeMessage.addRecipients(Message.RecipientType.TO, to.toString());
				mimeMessage.setSubject("[TO: " + mq.to_addresses + "]" + mq.subject);
				recipient = recipient + "#" + testMail;
			} else {
				mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(mq.to_addresses));
				mimeMessage.setSubject(mq.subject);
			}
			mimeMessage.setSentDate(new Date());
			
			Transport.send(mimeMessage);
			// DEVELOPMENT, pada mode development, tidak usah kirim tapi sleep beberapa saat
			mq.status = MAIL_STATUS.SENT; //sukses										
			mq.exception=null; //update status as SENT
			Logger.debug("[MAIL-JOB] MailSent [%s to %s] '%s'", mq.id, mq.to_addresses, mq.subject);
			success=true;
		}
	
		catch (Exception e) {
			// eror: java.net.UnknownHostException,
			// javax.mail.SendFailedException
			//java.net.ConnectException
			e.printStackTrace();
			mq.exception=e.toString();
			Logger.error("[MAIL-JOB] Error sending mail, subject: %s, %s", mq, mq.exception);
			mq.retry++;
			// jika gagal,status menjadi OUTBOX atau FAILED
			if (mq.retry >= retryCount)
				mq.status = MAIL_STATUS.FAILED;
			else
				mq.status = MAIL_STATUS.OUTBOX;
			if(mq.exception.contains("java.net.ConnectException"))
				success=false;// stop sending email for this session
			success=false;
		}
		save(mq);
		return success;
	}
	//simpan email via Job baru supaya jalan di Transaction yg baru
	private void save(MailQueue mq) {
		new Job()
		{
			public void doJob()
			{
				mq.save();
			}
		}
		.now();
	}



	

}
