package views.tags;

import groovy.lang.Closure;

import java.io.PrintWriter;
import java.util.Map;

import play.templates.GroovyTemplate.ExecutableTemplate;
import play.templates.JavaExtensions;
import play.templates.TagContext;

public class FastTags extends play.templates.FastTags {

	/**FastTag untuk. #{checked value/}
	 * Jika value=true akan diprint "checked".
	 * Ini digunakan di dalam radiobutton dan checkbox */
	public static void _checked(Map<?, ?> args, Closure body, PrintWriter out,
			ExecutableTemplate template, int fromLine) {
		Object value = args.get("arg");
		 if(value!=null)
		 {
			 if(value instanceof Boolean)
				 if((Boolean)value)
					 out.print(" checked ");
		 }
	}
}
