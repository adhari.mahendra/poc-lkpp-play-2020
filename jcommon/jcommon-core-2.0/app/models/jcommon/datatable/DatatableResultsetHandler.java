package models.jcommon.datatable;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.sql2o.ResultSetHandler;

/**Silakan extends dari ini.
 * Sebaiknya, HTML di-generate di sini dan tinggal di-render di browser supaya 
 * 1. mudah debugging 
 * 2. cepat karena support Cache  
 * @author Andik
 */
public abstract class DatatableResultsetHandler<T> implements ResultSetHandler{

	public String[] columns;
	/**Berisi nama-nama kolom yang akan ditampilkan di HTML
	 * @param columns
	 */
	public DatatableResultsetHandler(String columns) {
		this.columns=columns.split(",");
	}
	
	@Override
	//Coding untuk mendapatkan String[] dari sebuah row
	public String[] handle(ResultSet rs) throws SQLException {
		String[] results=new String[columns.length];
		for(int i=0; i<columns.length; i++)
			results[i]=rs.getString(columns[i]);
		return results;
	}

}
