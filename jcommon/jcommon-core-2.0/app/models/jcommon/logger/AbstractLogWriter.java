package models.jcommon.logger;

import java.util.concurrent.LinkedBlockingQueue;

import play.Logger;
import play.jobs.Job;

public abstract class AbstractLogWriter extends Job<Object> {

	private static final int JOB_EVERY_SECONDS=30;
	
	protected static LinkedBlockingQueue<ActivityLog> queue;

	/**
	 * Penulisan ke database dilakukan oleh thread tersendiri agar tidak
	 * mengganggu Request yang di-log
	 * 
	 * @param activityLog
	 */
	public synchronized static void write(ActivityLog activityLog) {
		if (queue == null) {
			queue = new LinkedBlockingQueue<ActivityLog>();
			// Create AbstractLogWriter with specific implementation
			AbstractLogWriter job=new GzipLogWriter();
			
			//RUN every n seconds
			job.every(JOB_EVERY_SECONDS);
			Logger.debug("[Log Writer] Writer Thread started");
		}
		queue.add(activityLog);
	}
	
	public abstract void doJob();

}
