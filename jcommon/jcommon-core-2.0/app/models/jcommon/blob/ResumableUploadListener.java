package models.jcommon.blob;

public abstract class ResumableUploadListener {

	public abstract void uploadDone(ResumableUpload um);
	public abstract void uploadFailed(ResumableUpload um, String message);
}
