package models.jcommon.secure.encrypt;

import play.mvc.Http.Request;

/** Class ini sebagai 'SERVER' yang menerima request dari client
 * 
 * @author andik
 *
 */
public class SecureWSServer extends SecureWS {

	public SecureWSServer(Request request) throws CipherEngineException {
		super(request);
	}

}
