package models.jcommon.secure.encrypt;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import play.libs.WS;
import play.libs.WS.HttpResponse;
import play.libs.WS.WSRequest;
import play.mvc.Http.Request;

import com.google.gson.Gson;

/** SecureWS merupakan utility untuk komunikasi via web service. 
 *  */
abstract class SecureWS {

	private static final String PARAM_KEY = "_PARAM_";
	private static final String PARAM_PLAIN_RESPONSE= "_PARAM_PLAIN_RESPONSE";
	private String url;
	private Map<String, String> params;
	private Boolean plainResponse=false;
	private HttpResponse postResponse;
	private String encJson;
	
	
	/**Contructor used to create SecureWS as 'Client' (make request) */
	protected SecureWS(String url)
	{
		this.url=url;
		plainResponse=false;
		params=new java.util.Hashtable();
	}
	
	protected SecureWS(String url, boolean plainResponse)
	{
		this.url=url;
		this.plainResponse=plainResponse;
		params=new java.util.Hashtable();
	}
	
	/**Contructor used to create SecureWS as 'Server' (accept request)
	 * @throws CipherEngineException */
	protected SecureWS(Request request) throws CipherEngineException {	
		String paramString=request.params.get(PARAM_KEY);	
		params=new java.util.Hashtable();
		//decrypt
		CipherEngine ce=new CipherEngine();
		String paramStringDecoded=new String(Base64.decodeBase64(paramString));
		String paramStringDecripted =ce.decryptString(paramStringDecoded);		
		params=new Gson().fromJson(paramStringDecripted, params.getClass());
		plainResponse=getParam(PARAM_PLAIN_RESPONSE, Boolean.class);
	}
	
	/**Tambahkan parameter dan langsung dikonvert menjadi json */
	public void addParam(String key, Object value)
	{
		Gson gson=new Gson();		
		params.put(key, gson.toJson(value));
	}
	
	public <T> T getParam(String key, Class<T> className)
	{
		Gson gson=new Gson();
		String value=params.get(key);
		return gson.fromJson(value, className);
	}
	
	/** Dapatkan parameter dengan tipe string */
	public String getParam(String key)
	{
		Gson gson=new Gson();
		String value=params.get(key);
		return gson.fromJson(value, String.class);
	}
	
	
	/**Digunakan untuk POST data ke server. Timeout menggunakan format
	 *  
	 * 
	 * @param timeout in seconds
	 * @return
	 * @throws CipherEngineException
	 * @throws InterruptedException
	 * @throws ExecutionException
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public HttpResponse doPost(int timeout) throws CipherEngineException, InterruptedException, ExecutionException, IOException
	{
		//************ using httpClient *********************
		//Play WS cannot be used if server using chunked response
//		BasicHttpParams httpParam=new BasicHttpParams();
//		httpParam.setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
//		HttpClient httpclient = new DefaultHttpClient();
//		HttpPost httpPost=new HttpPost(url);
		
		//tambahkan 'plain response'
		addParam(PARAM_PLAIN_RESPONSE, plainResponse);
		
		String encryptedParam = prepareRequestParameters();
//		
//		List nvps = new ArrayList();
//		nvps.add(new BasicNameValuePair(PARAM_KEY, encryptedParam));
//		
//		UrlEncodedFormEntity entity=new UrlEncodedFormEntity(nvps);
//		httpPost.setEntity(entity);
//		Logger.debug("[CLIENT] POST %s", url);		
		WSRequest request = WS.url(url).setParameter(PARAM_KEY, encryptedParam).setHeader("Content-Type", "application/x-www-form-urlencoded");
		request.timeout = timeout * 1000;		
		HttpResponse httpResponse= request.postAsync().get();
		postResponse=httpResponse;
		// check chunked response
		boolean chunked = "chunked".equals(httpResponse.getHeader("Transfer-Encoding"));
		if (chunked) {
			
		}
		return httpResponse;
//		InputStream is=responseEntity.getContent();
//		
//		Logger.debug("[CLIENT] Response Status: %s", httpResponse.getStatusLine());		
//		Logger.debug("[CLIENT] is chunked: %s", responseEntity.isChunked());
//		//baca
//		return is;
	}

	/**Siapkan request parameter (enkripsi) */
	private String prepareRequestParameters() throws CipherEngineException {
		//encrypt params
		String str=new Gson().toJson(params);	
		String strParam=str.toString();
//		Logger.debug("[CLIENT] Params: %s", strParam);
		CipherEngine ce=new CipherEngine();
		String strParamEnc=ce.encryptString(strParam);
		strParamEnc=Base64.encodeBase64URLSafeString(strParamEnc.getBytes());
		
		return strParamEnc;
		//return strParam;
	}
	
	/**Cek apakah web service ini harus return sebagai plain text atau tidak.
	 * Jika nilai true maka Controller tinggal melakukan render().
	 * Jika bernilai false maka text yang dirender harus di-enkripsi terlebih dahulu
	 * 
	 * @return
	 */
	public boolean isPlainResponse()
	{
		return plainResponse;
	}
	
	/** Dapatkan response sebagai string 
	 * @throws IOException 
	 * @throws IllegalStateException */
	public String getResponseAsString() throws IllegalStateException, IOException
	{
		String str=null;
		str = postResponse.getString();
		if(postResponse.getStatus() != 200)
			throw new IOException(String.format("Error accessing URL %s with error: %s", url, postResponse.toString()));
		if(!isPlainResponse())//decrypt jika WS pake encripsi
		{
			try {
				
				CipherEngine ce;				
				ce = new CipherEngine();
				str=ce.decryptString(str);
				return str;
				
			} catch (CipherEngineException e) {
				e.printStackTrace();
			}
		}
		
		return str;
	}
	
	/** Setting response sebagai plain string atau encrypted string */
	public String prepareResponse(String jsonResponse)
	{
		String strResponse = jsonResponse;
		if(!isPlainResponse())//encrypt jika response tidak plain
		{
			try {
								
				CipherEngine ce = new CipherEngine();
				String strParamEncrypt=ce.encryptString(jsonResponse);			
//				String strParamEncode = Base64.encodeBase64URLSafeString(strParamEncrypt.getBytes());			
				strResponse = strParamEncrypt;
			} 
			catch (CipherEngineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return strResponse;
	}
	
	/** Mengambil request paramater  */
	public Map getHttpParams()
	{		
		return this.params;
	}
	
	/** Dapatkan response sebagai encrypted string */
	public String getResponseAsEncString()
	{
		return encJson;
	}

}