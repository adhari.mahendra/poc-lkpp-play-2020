package models.jcommon.secure;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.time.StopWatch;

import play.Logger;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

@Table(name="SEC_KEY_PAIR")
public class KeyPairModel extends BaseTable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8775878975692369951L;

	/*Jumlah key pair yang disimpan
	 * 
	 */
	public static int  KEY_PAIR_POOL_SIZE=10;
	
	@Id(sequence="SEQ_SEC_KEY_PAIR")
	public Integer keyPairId;
	
	public String privateKey;
	
	public String publicKey;
	
	public Date creationTime;
	
	//berapa kali sudah digunakan
	public int usage;
	
	public KeyPairModel(KeyPair kp) throws InvalidKeySpecException, IOException
	{
		Key pubKey=kp.getPublic();
		publicKey=convertToString(pubKey);
		privateKey=convertToString(kp.getPrivate());
		usage=0;
		creationTime=new Date();
	}
	
	public KeyPairModel(String privateKey, String publicKey) {
		this.privateKey=privateKey;
		this.publicKey=publicKey;			
	}

	private String convertToString(Object obj) throws IOException
	{
		ByteArrayOutputStream ary=new ByteArrayOutputStream(10000);
		ObjectOutputStream oos=new ObjectOutputStream(ary);
		oos.writeObject(obj);
		
		String encoded=Base64.encodeBase64String(ary.toByteArray());
		return encoded;
	}
	
	private Object convertFromString(String objectStream) throws IOException, ClassNotFoundException
	{
		if(objectStream!=null)
		{
			byte[] buff=Base64.decodeBase64(objectStream);
			
			ObjectInputStream ois=new ObjectInputStream(new ByteArrayInputStream(buff));		
			return ois.readObject();
		}
		else
			return null;
	}
	
	//dapatkan public key
	public RSAPublicKey getPublicKeyObject() throws ClassNotFoundException, IOException
	{
		return (RSAPublicKey) convertFromString(publicKey);
	}
	
	public RSAPrivateKey getPrivateKeyObject() throws ClassNotFoundException, IOException
	{
		return (RSAPrivateKey) convertFromString(privateKey);
	}
	
	
	public String getPublicKeyAsHex() throws IOException, ClassNotFoundException
	{
		return getModulusAsHex(publicKey);
	}
	
	private String getModulusAsHex(String objKey) throws IOException, ClassNotFoundException
	{
		byte aryKey[]=Base64.decodeBase64(publicKey);
		ByteArrayInputStream is=new ByteArrayInputStream(aryKey);
		ObjectInputStream ois=new ObjectInputStream(is);
		Object obj=ois.readObject();
		Key key=(Key)obj;
		byte aryKeyEncode[]=key.getEncoded();
		String st=Hex.encodeHexString(aryKeyEncode);
		return st;
	}
	
	/** create keypairs
	 * 
	 * Usually on appStart
	 */
		public static void createKeyPairsStock()
		{
		
			KeyPairGenerator kpg;
			try {
				kpg = KeyPairGenerator.getInstance("RSA");
				kpg.initialize(1024);
				StopWatch sw=new StopWatch();
				sw.start();
				Logger.debug("[GEN-KEY STARTED] Generate KeyPairs Stock: %s", KeyPairModel.KEY_PAIR_POOL_SIZE);			
				for(int i=0; i<= KeyPairModel.KEY_PAIR_POOL_SIZE; i++)
				{
					KeyPair kp = kpg.genKeyPair();
					KeyPairModel kpm=new KeyPairModel(kp);
					kpm.save();
				}
				sw.stop();
				Logger.debug("[GEN-KEY DONE] Duration: %s", sw.toString());

			} catch (NoSuchAlgorithmException e) {
				Logger.error("Error create KeyPairGenerator.getInstance(\"RSA\") OnApplicationStart");
			} catch (InvalidKeySpecException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public static KeyPairModel findFirst()
		{
			List<KeyPairModel> list=findAll();
			if(list!=null)
				return list.get(0);
			return null;
		}
	
}
