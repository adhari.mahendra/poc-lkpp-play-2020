package models.jcommon.secure.encrypt2;

import java.io.IOException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;

import org.apache.commons.codec.binary.Base64;

import models.jcommon.secure.encrypt.CipherEngineException;
import models.jcommon.util.CompressionUtil;

public class InMemoryKeyCipherEngine extends CipherEngine2 {
	private boolean withCompress;

	public InMemoryKeyCipherEngine(int cipherMode, String idxKey, boolean withCompress) throws CipherEngineException {
		super(cipherMode, idxKey);
		this.withCompress = withCompress;
	}

	/**
	 * Method untuk membaca key dimodifikasi dengan kondisi sebagai berikut:
	 * Jika mode enkripsi, kunci yang digunakan haruslah kunci publik dari klien
	 * Jika mode dekripsi, kunci yang digunakan adalah kunci private yang disimpan di cache
	 * @param key jika mode enkripsi = kunci publik dari klien dalam format base 64. jika mode dekripsi = session key dari kunci private 
	 */
	@Override
	protected byte[] readKey(String key) throws IOException {
		// ambil dari RSA KEY
		if(isEncryptMode()) {
			return Base64.decodeBase64(key);
		} else {
			return Base64.decodeBase64(InMemoryRSAKey.getPrivate(key));
		}
	}
	
	public static InMemoryKeyCipherEngine getEncyptEngine(String sessionKey, boolean withCompress)  throws CipherEngineException {
		return new InMemoryKeyCipherEngine(Cipher.ENCRYPT_MODE, sessionKey, withCompress);
	}
	
	public static InMemoryKeyCipherEngine getDecyptEngine(String sessionKey, boolean withCompress)  throws CipherEngineException {
		return new InMemoryKeyCipherEngine(Cipher.DECRYPT_MODE, sessionKey, withCompress);
	}
	
	@Override
	public byte[] doCrypto(byte[] data) throws CipherEngineException {
		if(withCompress) {
			// dilakukan kompresi/dekompresi data terlebih dahulu
			if(isEncryptMode()) {
				return super.doCrypto(CompressionUtil.compress(data));
			} else {
				return CompressionUtil.decompress(super.doCrypto(data));
			}
		} else {
			return super.doCrypto(data);
		}
	}

}
