package models.jcommon.secure.encrypt2;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

import models.jcommon.secure.encrypt.CipherEngineException;

/**
 * Ini sebagai pengganti CipherEngine.
 * Class ini berfungsi sebagai encrypt/Decript tergantung mode saat constructor
 * @author andik
 * 
 * Class ini bekerja dengan menggunakan enkripsi 2 langkah
 * Langkah #1
 * 	  a. RSA Publik key digunakan untuk enkripsi 256byte AES_KEY
 *    b. #1a dimasukkan ke dalam output sebagai 256byte awal
 * Langkah #2
 *    key pada #1a digunakan untuk melakukan enkripsi data yang sebenarnya
 * Jadinya sebagai berikut:
 * 	[256byte-aes-encrypted-key][actual-data]
 * 
 * Proses dekripsi dilakukan dengan 2 langkah pula
 * Langkah #1
 *    a. Membaca 256 byte pertama 
 *    b. Melakukan dekripsi data #1a menggunakan RSA private key -> didapatkan AES_KEY
 * Langkah #2
 *    melakukan dekripsi byte-byte berikutnya menggunakan AES_KEY 
 */
public abstract class CipherEngine2 {
	
	public static final String VERSION="2.0.0";
	
	private Key key;
	private Cipher cipher1;// asym to encrypt a key
	private int cipherMode;

	public static final String ALGORITHM = "RSA/ECB/PKCS1Padding";

	static {
		java.security.Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	}

	/**Create class ini dengan mode ENCRYPT atau DECRYPT menggunakan key yg ada
	 * @param cipherMode
	 * @param cipherkey 
	 * @throws CipherEngineException
	 */
	public CipherEngine2(int cipherMode, String cipherkey) throws CipherEngineException {
		this.cipherMode = cipherMode;
		try
		{
			if (cipherMode == Cipher.ENCRYPT_MODE) {
				key = getPublicKey(cipherkey);
				cipher1 = Cipher.getInstance(ALGORITHM, "BC");
				cipher1.init(Cipher.ENCRYPT_MODE, key);
			} else {
				key = getPrivateKey(cipherkey);
				cipher1 = Cipher.getInstance(ALGORITHM, "BC");
				cipher1.init(Cipher.DECRYPT_MODE, key);
			}
		}
		catch(Exception e)
		{
			throw new CipherEngineException(e);
		}
	}

	private PrivateKey getPrivateKey(String key) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] keyBytes = readKey(key);
		PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		PrivateKey priv = kf.generatePrivate(spec);
		return priv;
	}

	private Key getPublicKey(String key) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
		byte[] keyBytes = readKey(key);
		X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
		KeyFactory kf = KeyFactory.getInstance("RSA");
		return kf.generatePublic(spec);
	}

	/**
	 * Read key
	 * 
	 * @param cipherKey
	 * @return
	 * @throws IOException
	 */
	protected byte[] readKey(String cipherKey) throws IOException {
		byte[] keyBytes = Base64.decodeBase64(cipherKey);
		return keyBytes;
	}

	
	/**Melakukan encrypt/decrypt data berupa byte[]
	 * 
	 * @param data
	 * @return
	 * @throws CipherEngineException
	 */
	public byte[] doCrypto(byte[] data) throws CipherEngineException {
		if(cipherMode==Cipher.ENCRYPT_MODE)
		{
			ByteArrayOutputStream os=new ByteArrayOutputStream();
			encrypt(new ByteArrayInputStream(data), os);
			return os.toByteArray();
		}
		else
			return decrypt(data);
	}
	
	private void encrypt(InputStream is, OutputStream out) throws CipherEngineException
	{
		try {
			KeyGenerator generator = KeyGenerator.getInstance("AES", "BC");
			generator.init(256);
			SecretKey secretKey = generator.generateKey();
			byte[] aesKey = secretKey.getEncoded();
			byte encryptedKey[] = cipher1.doFinal(aesKey);
			Cipher cipher2 = Cipher.getInstance("AES", "BC");
			SecretKeySpec skeySpec = new SecretKeySpec(aesKey, "AES");
			cipher2.init(cipherMode, skeySpec);
			out.write(encryptedKey);
			while(true)
			{
				byte[] buff=new byte[1024];
				int count=is.read(buff, 0, 1024);
				if(count<=0)
					break;
				byte[] result = cipher2.update(buff, 0, count);
				if(result!=null)
					out.write(result);;
			}
			byte result[] = cipher2.doFinal();
			out.write(result);
			
		} catch (Exception e) {
			throw new CipherEngineException(e);
		}
	}
	
	//first 256 byte is key
	private byte[] decrypt(byte[] data) throws CipherEngineException
	{
		InputStream is=new ByteArrayInputStream(data);
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		decrypt(is, out);
		return out.toByteArray();
	}
	
	private void decrypt(InputStream is, OutputStream out) throws CipherEngineException
	{
		byte[] aesKey=new byte[256]; 
		try
		{
			is.read(aesKey, 0, 256);
			//decrypt the key
			aesKey=cipher1.doFinal(aesKey);
			
			//read actual data
			Cipher cipher2 = Cipher.getInstance("AES", "BC");
			SecretKeySpec skeySpec = new SecretKeySpec(aesKey, "AES");
			cipher2.init(cipherMode, skeySpec);
			byte[] buff=new byte[1024];
			while(true)
			{
				int count=is.read(buff);
				if(count<=0)
					break;
				byte[] result=cipher2.update(buff,0, count);
				if(result!=null)
					out.write(result);
			}
			byte[] result=cipher2.doFinal();
			out.write(result);
		}
		catch(NoSuchPaddingException | NoSuchProviderException | NoSuchAlgorithmException |  javax.crypto.BadPaddingException | IllegalBlockSizeException | InvalidKeyException e)
		{
			throw new CipherEngineException("Invalid Decryption: " + e.getMessage());
		}
		catch(IOException e)
		{
		} 
		
	}

	/**Melakukan crypto pada String. Untuk ukuran data besar, jangan pakai ini; pakai yg Stream
	 * @param data
	 * @return
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws CipherEngineException 
	 */
	public String doCrypto(String data) throws IllegalBlockSizeException, BadPaddingException, CipherEngineException {
		byte[] buff = doCrypto(data.getBytes());
		return new String(buff);
	}

	/**Melakukan crypto pada inputstream dengan output sebagai stream
	 * @param is
	 * @param out
	 * @throws IOException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws CipherEngineException 
	 */
	public synchronized void doCrypto(InputStream is, OutputStream out)
			throws IOException, IllegalBlockSizeException, BadPaddingException, CipherEngineException {
		if(cipherMode==Cipher.DECRYPT_MODE)
			decrypt(is, out);
		else
			encrypt(is, out);
	}

	protected boolean isEncryptMode() {
		return cipherMode == Cipher.ENCRYPT_MODE;
	}

	protected boolean isDecryptMode() {
		return cipherMode == Cipher.DECRYPT_MODE;
	}
}
