package models.jcommon.secure.encrypt2;

import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;

import models.jcommon.secure.encrypt.CipherEngineException;
import play.cache.Cache;

/**Class ini berfungsi untuk generate dan menyimpan key RSA yang digunakan di CipherEngine2 
 * @author idoej
 */
public class CachedRSAKey {
	public PublicKey publicKey;
	public PrivateKey privateKey;
	public String sessionKey;
	/**
	 * Constructor dengan generate private/public key di memory dan disimpan di cache tanpa salt
	 * @throws CipherEngineException
	 */
	public CachedRSAKey() throws CipherEngineException {
		this(null);
	}
	
	/**
	 * Constructor dengan generate private/public key di memory dan disimpan di cache
	 * @param salt salt untuk tambahan session key di cache
	 * @throws CipherEngineException
	 */
	public CachedRSAKey(String salt) throws CipherEngineException {
		this.generateKeyPair();
		String prefix = "";
		if(salt != null) prefix = salt + "_";
		this.sessionKey = prefix+UUID.randomUUID().toString();
		this.save();
	}

	
	/** 
	 * Generate in memory public/private key 
	 * @throws CipherEngineException 
	 * @throws IOException 
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchProviderException */
	private void generateKeyPair() throws CipherEngineException {
		try {
			KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
			kpg.initialize(2048);
			KeyPair kp= kpg.generateKeyPair();

			this.privateKey = kp.getPrivate();
			this.publicKey = kp.getPublic();
		} catch(Exception e) {
			throw new CipherEngineException(e);
		}
	}
	
	/**
	 * Method untuk menyimpan ke cache
	 */
	private void save() {
		Cache.set("public_key_"+this.sessionKey, Base64.encodeBase64String(this.publicKey.getEncoded()), "5mn");
		Cache.set("private_key_"+this.sessionKey, Base64.encodeBase64String(this.privateKey.getEncoded()), "5mn");
	}
	
	/**
	 * Method untuk menghapus dari cache
	 */
	public void delete() {
		Cache.delete("public_key_"+this.sessionKey);
		Cache.delete("private_key_"+this.sessionKey);
	}
	
	/**
	 * Method untuk menghapus dari cache
	 */
	public static void delete(String sessionKey) {
		Cache.delete("public_key_"+sessionKey);
		Cache.delete("private_key_"+sessionKey);
	}
	
	/**
	 * Method untuk mengambil public key dari cache berdasarkan sessionKey
	 * @param sessionKey key untuk mendapatkan engine
	 * @return public key dalam format base 64
	 */
	public static String getPublic(String sessionKey) {
		return (String)Cache.get("public_key_"+sessionKey);
	}
	
	/**
	 * Method untuk mengambil private key dari cache berdasarkan sessionKey
	 * @param sessionKey key untuk mendapatkan engine
	 * @return private key dalam format base 64
	 */
	public static String getPrivate(String sessionKey) {
		return (String)Cache.get("private_key_"+sessionKey);
	}
	
	/**
	 * get base 64 encoded public key
	 * @return
	 */
	public String getPublicKeyEncoded() {
		return Base64.encodeBase64String(this.publicKey.getEncoded());
	}
	
	/**
	 * get base 64 encoded private key
	 * @return
	 */
	public String getPrivateKeyEncoded() {
		return Base64.encodeBase64String(this.privateKey.getEncoded());
	}
}
