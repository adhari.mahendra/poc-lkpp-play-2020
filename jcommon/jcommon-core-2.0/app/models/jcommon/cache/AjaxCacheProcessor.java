package models.jcommon.cache;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public interface AjaxCacheProcessor {
	
	public String process();
}
