package views.tags;

import groovy.lang.Closure;

import java.io.File;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.io.StringReader;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.Map;

import play.Play;
import play.templates.FastTags;
import play.templates.GroovyTemplate.ExecutableTemplate;

/**Fast tag khusus untuk debugging.
 *  */
public class DebugTag extends FastTags {

	
	/**Menampilkan  

	 * @param body
	 * @param out
	 * @param template
	 * @param fromLine
	 * @throws IOException 
	 */
	public static void _debugLogger(Map<?, ?> args, Closure body, PrintWriter out, ExecutableTemplate template, int fromLine) throws IOException
	{
		String exceptionId=(String) args.get("exceptionId");
		File file=new File(Play.applicationPath + "/logs/system.out");	
		if(!file.exists())
		{
			StringBuilder strErrorContent=new StringBuilder();
			strErrorContent.append("<pre style=\"color:red\">Kode error")
			.append(exceptionId)
			.append(" tidak ditemukan pada log file. Silakan lihat console.</pre>");
			out.print(strErrorContent);
			return;
		}
		
			
		long fileSize=file.length();
		int bufferSize=exceptionId.length();
		long position=fileSize-bufferSize;
		
		//************************ STEP 1: dapatkan pada byte ke berapa exeptionId?		
		byte[] byteArray=new byte[bufferSize];
		boolean found;
		int byteRead=0;
		String lastStringData = "";
		try (RandomAccessFile raf = new RandomAccessFile(file, "r")) {
			FileChannel fc = raf.getChannel();
			do {
				MappedByteBuffer mbb = fc.map(MapMode.READ_ONLY, position, bufferSize);
				mbb.get(byteArray);
				// convert to string
				String currentString = new String(byteArray);
				found = currentString.equals(exceptionId);
				position--;
				byteRead++;
				if (byteRead == 102400) // DIBATASI, jika sampai 100KB terakhir
										// tidak ditemukan maka return
					break;
			} while (!found && position > 0);
			// jika tidak ketemu maka return
			if (!found) {
				StringBuilder strErrorContent = new StringBuilder();
				strErrorContent.append("<pre style=\"color:red\">Kode error: ").append(exceptionId)
						.append(" tidak ditemukan pada log file. Silakan lihat console.</pre>");
				out.print(strErrorContent);
				return;
			}

			// ************** STEP 2: Baca seluruh byte mulai 'position' hingga
			// akhir, simpan pada string
			bufferSize = (int) (fileSize - position);
			MappedByteBuffer mbb = fc.map(MapMode.READ_ONLY, position, bufferSize);
			byteArray = new byte[bufferSize];
			mbb.get(byteArray);
			lastStringData = new String(byteArray);
		}
		
		LineNumberReader reader=new LineNumberReader(new StringReader(lastStringData));

		String st=null;
		int errorLineNumber=-1;
		StringBuilder strErrorContent=new StringBuilder(5000);
		strErrorContent.append(
			"<style>")
				.append(".line {clear: both;color: #333;margin-bottom: 1px;}")
				.append("pre {font-size: 14px;margin: 0;overflow-x: hidden; }")
			.append("</style>");
		int line=0;
		
		//tampilkan secara formatted
		while((st=reader.readLine())!=null)
		{			
			if(st.equals(exceptionId))
				errorLineNumber=1;//error has been found, so write data
			if(errorLineNumber!=-1)
			{
				line++;
				strErrorContent.append("<span class=line>");
					if(line==4)
						strErrorContent.append("<pre style=\"color:red\">");
					else
						strErrorContent.append("<pre>");
					strErrorContent.append(st);
					strErrorContent.append("</pre>");
				strErrorContent.append("</span>");
			}
		}
		
			out.print(strErrorContent);
	}
}
