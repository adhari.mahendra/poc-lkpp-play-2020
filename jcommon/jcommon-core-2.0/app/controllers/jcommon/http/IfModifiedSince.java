package controllers.jcommon.http;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface IfModifiedSince {
	/**value berisi berapa lama sebuah Page di-Cache oleh browser
	 * @return
	 */
	String value()  default "1min"; 
}
