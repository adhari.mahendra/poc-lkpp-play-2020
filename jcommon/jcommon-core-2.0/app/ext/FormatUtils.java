package ext;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.joda.time.Period;

import models.jcommon.util.CommonUtil;
import play.Logger;
import play.templates.JavaExtensions;

/**
 * Kelas {@code FormatUtils} merupakan kumpulan fungsi-fungsi yang berguna untuk <i>formatting</i> String
 *
 * @author Andik Yulianto
 * @author Arief Ardiansyah
 * @author I Wayan Wiprayoga W
 */
public class FormatUtils extends JavaExtensions{
	/**
	 * Nilai MEGABYTE dalam BYTE
	 */
	public static final long  MEGABYTE = 1024 * 1024;
	/**
	 * Nilai KILO BYTE dalam BYTE
	 */
	public static final long  KILOBYTE = 1024 ;
	/**
	 * Indonesia Locale
	 */
	public static final Locale indonesia = new Locale("ID","id");
	/**
	 * Default format tanggal (LONG Format)
	 */
	public static final DateFormat defaultDateFormat =  DateFormat.getDateInstance(DateFormat.LONG, indonesia);
	/**
	 * Default format waktu (SHORT Format)
	 */
	public static final DateFormat defaultTimeFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.UK);
	/**
	 * Format Date Indonesia
 	 */
	public static final DateFormat sdf_indonesian = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.SHORT, new Locale("id","ID"));
	/**
	 * Digunakan untuk formatting tanggal TTS
	 */
	public static final SimpleDateFormat ttsDateFormat = new SimpleDateFormat("ddMMyy");
	public static final String[] day = new String[]{"Minggu","Senin","Selasa","Rabu","Kamis","Jum'at", "Sabtu"};
	public static final String[] month = new String[]{"Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"};
	/**
	 * format Locale indonesia 
	 * jika setting format waktu dan time zone, ini sangat penting untuk digunakan
	 */
	public static final Locale LOCALE_INDONESIA = new Locale("ID","id");
	
	/**
	 * format area zone waktu indonesia
	 */
	public static final TimeZone TIMEZONE_INDONESIA = Calendar.getInstance(FormatUtils.LOCALE_INDONESIA).getTimeZone();
	
	/**
	 * setting format nilai offset berdasarkan zone waktu di atas
	 * indonesia ada di wilayah waktu GMT+7, maka nilai offsetnya +7
	 */
	public static final int TIMEZONE_OFFSET = TIMEZONE_INDONESIA.getRawOffset()/3600000;
	
	public static final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
	
	

	/**
	 * Fungsi {@code formatFileSize} digunakan untuk mendapatkan representasi string ukuran file
	 * dalam bentuk (Byte/KiloByte/MegaByte)
	 * @param ukuran nilai ukuran (tipe data {@code Long})
	 * @return ukuran file dalam format [ukuran][B/KB/MB]
	 */
	public static String formatFileSize(Long data) {
		StringBuilder str=new StringBuilder();
		if(data < 1024)
			str.append(formatDesimal(data)).append(" bytes");
		else
		{
			data=data/1024;
			if(data < 1024)
				str.append(formatDesimal(data)).append(" KB");
			else
			{
				data=data/1024;
				if(data < 1024)
					str.append(formatDesimal(data)).append(" MB");
				else
					str.append(formatDesimal(data/1024)).append(" GB");
			}				
		}
		return str.toString();
	}
	
	public static String formatBytes(Long data) {
		return formatFileSize(data);
	}

	public static String formatBulan(Integer bulan){
		if( bulan!=null && bulan!=0 ){
			String namaBulan = month[bulan-1];
			return namaBulan;
			}
		return " ";
		
	
	}
	/**
	 * Fungsi {@code formatCurrenciesJuta} digunakan untuk format angka ke dalam bentuk (Juta,Milyar,Trilyun)
	 * @param number nilai yang diformat
	 * @return nilai {@code number} yang sudah terformat dalam bentuk [nilai][jt/M/T]
	 */
	public static String formatCurrenciesJuta(Number number) {
		if(number==null)
			return "";
		String hasil = "";
		String sufix = "";
		if (number != null) {
			double num = number.doubleValue();
			// double juta = num / 1E6;
			double milyar = num / 1E9;
			double trilyun = num / 1E12;
			// pakai juta atau milyar?
			if (milyar < 1) {
				sufix = " Jt";
				num /= 1E6;
			} 
			else if (milyar >= 1 && trilyun < 1) {
				sufix = " M";
				num /= 1E9;
			}
			else {
				sufix = " T";
				num /= 1E12;
			}
			NumberFormat format = new DecimalFormat("###,###,###,###,###,##0.#");
			hasil = format.format(num);
			// hasil = "Rp " + hasil; //tanpa Rp
			hasil = hasil.replaceAll(",", "_");
			hasil = hasil.replaceAll("\\.", ",");
			hasil = hasil.replaceAll("_", "\\.");
			
		}
		return hasil + sufix;
	}

	/**
	 * Fungsi {@code formatCurrenciesJutaRupiah} digunakan untuk format angka kedalam bentuk [Rp. ][nilai][jt/M/T].
	 *
	 * @param number nilai yang diformat
	 * @return nilai {@code number} yang sudah terformat dalam bentuk [Rp. ][nilai][jt/M/T]
	 */
	public static String formatCurrenciesJutaRupiah(Number number) {
		if(number == null)
			return "";
		String hasil = formatCurrenciesJuta(number);
		return "Rp " + hasil;
	}

	/**
	 * Fungsi {@code formatCurrencyRupiah} digunakan untuk format angka kedalam bentuk Rupiah
	 * @param number nilai yang diformat
	 * @return nilai {@code number} yang sudah terformat dalam bentuk Rp. [nilai]
	 */
	public static String formatCurrencyRupiah(Number number) {
		String hasil = "";
		if (number != null) {
			NumberFormat format = NumberFormat.getCurrencyInstance(indonesia);
			format.setMaximumFractionDigits(0);
			hasil = format.format(number).replaceAll("Rp", "Rp ");
		}
		return hasil;
	}

	/**
	 * Fungsi {@code formatCurrency} digunakan untuk format angka kedalam bentuk currency
	 * @param number nilai yang diformat
	 * @return nilai {@code number} yang sudah terformat
	 */
	public static String formatCurrency(Number number) {
		String hasil = "";
		if (number != null) {
			NumberFormat format = NumberFormat.getInstance();
			hasil = format.format(number);
		}
		return hasil;
	}
	

    /**
     * Format tanggal sesuai dengan format tanggal yang ditampilkan oleh JQUERY-Date-Picker
     * @param date tanggal
     * @return string format tanggal
     */
    public static String formatDateToDatePickerView(Date date) {
        if (date != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            return formatter.format(date);
        }
        return "";
    }

    /**
     * Format tanggal database dari format tanggal yang ditampilkan oleh JQUERY-Date-Picker
     * @param date tanggal
     * @return objek Date
     */
    
    
    public static Date formatDateFromDatePickerView(String date) {
    	if(date==null)
    		return null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            play.Logger.debug("At FormatUtils.formatDateFromDatePickerView -> Tidak dapat melakukan parsing tanggal!");
        }
        return null;
    }
    
    
    /**
     * Format tanggal database dari format tanggal yang ditampilkan oleh JQUERY-Date-Picker
     * @param date tanggal
     * @return objek Date
     */
    
    public static Date formatDateFromDatePicker(String date) {
    	if(date==null)
    		return null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            play.Logger.debug("At FormatUtils.formatDateFromDatePickerView -> Tidak dapat melakukan parsing tanggal!");
        }
        return null;
    }
    /**
     * Format tanggal sesuai dengan format tanggal yang ditampilkan oleh JQUERY-DateTime-Picker
     * @param date tanggal
     * @return string format tanggal
     */
    public static String formatDateToDateTimePickerView(Date date) {
        if (date != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy  hh:mm");
            return formatter.format(date);
        }
        return "";
    }

    /**
     * Format tanggal database dari format tanggal yang ditampilkan oleh JQUERY-DateTime-Picker
     * @param date tanggal
     * @return objek Date
     */
    public static Date formatDateFromDateTimePickerView(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        try {
            return formatter.parse(date);
        } catch (ParseException e) {
            play.Logger.debug("At FormatUtils.formatDateFromDateTimePickerView -> Tidak dapat melakukan parsing tanggal!");
        }
        return null;
    }

	// TODO: Dokumentasi needed!
	public static String formatDateIndWithDay(Date date){	
		if (date != null) {
			return defaultDateFormat.format(date).replaceFirst(" ", ", ");
		}
		return "";
	}
	
	
	/**
	 * Fungsi {@code formatDateInd} digunakan untuk format tanggal sesuai region Indonesia (LONG Format)
	 * @param date objek {@link Date}
	 * @return string tanggal terformat sesuai region Indonesia
	 */
	public static String formatDateInd(Date date){
		if(date != null)
			return defaultDateFormat.format(date);
		return "";
	}

	/**
	 * Fungsi {@code formatDateTimeInd} digunakan untuk format tanggal dan waktu sesuai region Indonesia (d MMM yyyy HH:mm)
	 * @param date tanggal + waktu yang diformat
	 * @return string tanggal + waktu terformat
	 */
	public static String formatDateTimeInd(Date date){		
		return formatDateInd(date) + " " + formatTime(date);
	}


	/**
	 * Fungsi {@code formatDateTimeSecond} digunakan untuk format tanggal + waktu ke
	 * dalam bentuk tanggal + waktu (termasuk detik)
	 * @param date tanggal yang diformat
	 * @return string tanggal + waktu terformat
	 */
	public static String formatDateTimeSecond(Date date) {
		if (date != null) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return format.format(date);
		}
		return "";
	}

	/**
	 * Fungsi {@code formatTime} digunakan untuk format waktu ke region Indonesia (SHORT Format)
	 * @param date tanggal berisi informasi waktu yang diformat
	 * @return string waktu terformat
	 */
	public static String formatTime(Date date) {
		if (date != null) {
			return defaultTimeFormat.format(date);
		}
		return "";
	}

	/**
	 * Fungsi {@code formatDateNoYear} digunakan untuk format tanggal tanpa menampilkan tahun
	 * @param date tanggal yang diformat
	 * @return string tanggal terformat
	 */
	public static String formatDateNoYear(Date date) {
		if (date != null) {
			SimpleDateFormat format = new SimpleDateFormat("d MMM");
			return format.format(date);
		}
		return "";
	}

	/**
	 * Fungsi {@code formatDesimal} digunakan untuk format angka kedalam bentuk desimal (format ribuan dan pecahan)
	 * @param input nilai yang diformat
	 * @return string terformat dalam bentuk ###,###.###
	 */
	public static String formatDesimal(Number input){
		DecimalFormat df = new DecimalFormat("###,###,###,###,###,###,###,###,###,###,###,###,##0",  DecimalFormatSymbols.getInstance(new Locale("id")));
		return df.format(input).replace(',', '.');
	}
	
	/**
	 * Fungsi {@code formatDesimal} digunakan untuk format angka kedalam bentuk desimal (format ribuan dan pecahan)
	 * @param input nilai yang diformat
	 * @return string terformat dalam bentuk ###,###,###,###,##0.00 []
	 */
	public static String formatDesimal2(Number input){
		DecimalFormat df = new DecimalFormat("###,###,###,###,###,###,###,###,###,###,###,###,##0.00");
		return df.format(input);
	}
	
	/**Format dengan US locale
	 * 
	 * @param input
	 * @return
	 */
	public static String formatDesimal2US(Number input){
		DecimalFormat df = new DecimalFormat("###,###,###,###,##0.00", DecimalFormatSymbols.getInstance(Locale.US));
		return df.format(input);
	}

	// TODO: Dokumentasi needed!
	public static String formatNumber(Number number) {
		String hasil = "";
		if (number != null) {
			return NumberFormat.getInstance().format(number);
		}
		return hasil;
	}

	/**
	 * Fungsi {@code bytesToMeg} digunakan untuk mengubah nilai dari Byte ke MegaByte
	 * @param bytes nilai dalam Bytes
	 * @return nilai dalam Mega Bytes
	 */
	public static long bytesToMeg(long bytes) {		
		return bytes / MEGABYTE ;
	}

	/**
	 * Fungsi {@code bytesToKilo} digunakan untuk mengubah nilai dari Byte ke KiloByte
	 * @param bytes nilai dalam Bytes
	 * @return nilai dalam Kilo Bytes
	 */
	public static long bytesToKilo(long bytes) {		
		return bytes / KILOBYTE ;
	}

	/**
	 * @deprecated sama fungsinya dengan {@link #formatFileSize(Long) formatFileSize}
	 * @param bytes nilai ukuran dalam Bytes
	 * @return ukuran file dalam format [ukuran][B/KB/MB]
	 */
	@Deprecated
	public static String byteToString(long bytes){
		String byteString=null;
		if (bytes < 1024)
			byteString = String.valueOf(bytes) + " byte";
		else if (bytes < FormatUtils.MEGABYTE)
			byteString = String.valueOf(bytesToKilo(bytes)) + " KB";
		else
			byteString = String.valueOf(bytesToMeg(bytes)) + " MB";
		return byteString;
	}

	/**
	 * @deprecated seharusnya untuk menghitung selisih hari menggunakan {@link org.joda.time.DateTime}
	 * @param date1 tanggal pertama
	 * @param date2 tanggal kedua
	 * @return selisih hari
	 */
	@Deprecated
	public static Integer selisihHari(Date date1, Date date2){
		return (int) ((date1.getTime() - date2.getTime()) / (24 * 3600 * 1000));
	}	
	
	static final String[] numbers = {"se", "dua ", "tiga ", "empat ", "lima ", "enam ", "tujuh ", "delapan ", "sembilan "}; 
    static final String[] levels = {"ribu ", "juta ", "milyar ", "trilyun "};
	/**
	 * Konversi dari Curreny ke String (Locale Indonesia)
	 *  /TODO masih ada bug contoh pada angka: 277.656.610
	 * @param number nilai yang diformat (dianggap long)
	 * @return String
	 */
	public static String number2Word(Number number) { 		
        StringBuilder result = new StringBuilder();    
        String str = String.valueOf(number.longValue()); 
        int mod = str.length() % 3; 
        int len = str.length() / 3; 
        if (mod>0) {
			len++;
		} else {
			mod = 3;
		}
        int begin = 0; 
        int end = mod; 
        try
        {
	        for (int i = 0; i < len; i++) {
	            int level = len-i; 
	            String val = str.substring(begin, end); 
	            int value = Integer.parseInt(val); 
	            int length = val.length(); 
	            for (int j = 0; j < length; j++) {
	                int num = parseInt(val.charAt(j)); 
	                switch(length-j) {
						case 3:
							if (num > 0) {
								result.append(numbers[num-1]).append("ratus ");
							}
							break;
	                    case 2: 
	                        if (num > 1) {
								result.append(numbers[num-1]).append("puluh ");
							} else if (num == 1) {
								result.append(numbers[parseInt(val.charAt(++j))-1]).append("belas ");
							}
	                        break; 
	                    case 1: 
	                        if (num > 1 || (level == 2 && value == 1)) {
								result.append(numbers[num-1]);
							} else if (num == 1) {
								result.append("satu ");
							}
	                        break;
					}
	            } 
	
	            if (level > 1 && value > 0) {
					result.append(levels[level-2]);
				}
	            begin = i*3 + mod; 
	            end += 3; 
	        }
        }
        catch(Exception e)
        {
        	Logger.error(e, "Error parsing number2Word: %s", str);
        	return "";
        }
	        
        return result.toString(); 
    }

	/**
	 * Fungsi {@code parseInt} digunakan untuk parsing karakter ke integer
	 * @param c karakter yang di-parsing
	 * @return hasil parsing
	 */
	private static int parseInt(char c) {
        int result = c - 48; 
        if (result < 0 || result > 9) {
			throw new NumberFormatException("For input char: '"+c+"'");
		}
        return result; 
    }

	/**
	 * Fungsi {@code formatDate} digunakan untuk format dua tanggal sekaligus sesuai region Indonesia.
	 * Biasanya dipakai untuk format waktu awal dan akhir.
	 * @param date1 tanggal awal
	 * @param date2 tanggal akhir
	 * @return string tanggal terformat
	 */
	public static String formatDate(Date date1, Date date2) {
		if(date1 == null && date2 == null)
			return "-";
		else if (date2 == null)
			return formatDateInd(date1);
		else {
			return formatDateInd(date1) + " - "+ formatDateInd(date2);
		}
	}
	
	   public static String formatPeriod(Period period)
	   {
		   StringBuilder str=new StringBuilder();
		   if(period.getHours() > 0)
			   str.append(period.getHours()).append(" jam");
		   if(period.getMinutes() > 0)
			   str.append(period.getMinutes()).append(" menit");
			str.append(period.getSeconds());
		   if(period.getMillis() > 0)
			   str.append(".").append(period.getMillis());
		   str.append(" detik");
		   return str.toString();
	   }
	   

		
		public static Date toDate(String dateString) {			
			Date dt = null;
			try {
				Date date = dateFormatter.parse(dateString);
				dt = date;
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return dt;
		}		   
		
		// parsing angka rupiah / format mata uang ke dalam Double
		public static Double parseRupiah(String value) {
			if(CommonUtil.isEmpty(value))
				return null;
			if (value.matches("^(Rp)?\\s*[0-9\\.]*,?[0-9]*$")) {
				value = value.replaceAll("[(Rp)\\.\\s]", "");
				value = value.replaceAll(",", ".");
			}
			return Double.valueOf(value);
		}

}
