package jobs;

import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTable.ARCHIEVE_MODE;
import models.jcommon.blob.BlobTableDao;
import play.Logger;
import play.jobs.Job;
import play.libs.WS;
import play.libs.WS.WSRequest;

public class CDNDownloadJob extends Job {

	private WSRequest req;
	private String md5Hash;
	private String uploadTicket;
	public CDNDownloadJob(String urlToDownload, String uploadTicket, String md5Hash)
	{
		String url=urlToDownload + "/" + uploadTicket;
		req= WS.url(url).timeout("5min");
		Logger.debug("Download & Copy file from URL: %s, MD5: %s", url, md5Hash);			
		this.md5Hash=md5Hash;
		this.uploadTicket=uploadTicket;
	}
	
	public void doJob()
	{
		
		play.libs.WS.HttpResponse resp;
		try {
			resp = req.get();
			if(resp.success())
			{
				String fileName=resp.getHeader("Content-Disposition");// contains=> attachment; filename="website Inaproc 2.psd"
				Pattern p=Pattern.compile("attachment; filename=\"(.*)\"");
				Matcher m= p.matcher(fileName);
				if(m.matches())
					fileName=m.group(1);
				else
					fileName=UUID.randomUUID().toString();
				BlobTable blob=BlobTableDao.saveInputStream(ARCHIEVE_MODE.ARCHIEVE, resp.getStream(), fileName);
				if(!blob.blb_hash.equalsIgnoreCase(md5Hash))
					Logger.debug("MD5 value mismatch for file %s. Client: %s - CDN: %s", uploadTicket, blob.getFile(), md5Hash);
				else
					Logger.debug("Download completed, saved to: %s, filename: %s", blob.getClass(), blob.getFileName());
			}
			else
				Logger.error("ERROR: %s", resp.getStatusText());

		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	
	}
}
