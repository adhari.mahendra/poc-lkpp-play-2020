package jobs;

import java.sql.SQLException;

import models.jcommon.cache.JcommonEhCache;
import models.jcommon.db.base.BaseDomainObject;
import models.jcommon.logger.GzipLogWriter;
import play.Logger;
import play.Play;
import play.cache.Cache;
import play.db.DB;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

@OnApplicationStart(async = false)
public class JCommonStartup extends Job<Object> {

	public void doJob() {

		// init Cache, set using custom cache
		Cache.cacheImpl = JcommonEhCache.newInstance();

		String dbURL;
		try {
			dbURL = DB.getConnection().getMetaData().getURL();
			if (dbURL.contains("mysql"))
				BaseDomainObject.isMySql = true;
			if (dbURL.contains("postgres"))
				BaseDomainObject.isPostgres = true;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		TempFileCleanupJob job = new TempFileCleanupJob();
		String temporaryFileCleanup = Play.configuration.getProperty("temp.file.cleanup", "12h");
		String temporaryFileAge = Play.configuration.getProperty("temp.file.age", "24h");
		Logger.info("temp.file.cleanup= every %s", temporaryFileCleanup);
		Logger.info("temp.file.age= %s", temporaryFileAge);
		job.every(temporaryFileCleanup);

		// pastikan file semua file log diarsip via Job
		new Job() {
			public void doJob() {
				GzipLogWriter logWriter = new GzipLogWriter();
				try {
					logWriter.doArchiveAllAsGzip();
				} catch (Exception e) {
					Logger.error(e, "Error during startup");

				}
			}
		}.in("1s");

	}
}
