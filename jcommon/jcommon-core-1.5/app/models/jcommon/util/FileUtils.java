package models.jcommon.util;

import java.io.File;

public class FileUtils {

    public static String sanitizedFilename(File file) {
        String text = file.getName().replaceAll("[^a-zA-Z0-9.\\s]", "");
        String path = file.getAbsolutePath().substring(0, file.getAbsolutePath().lastIndexOf(File.separator));
        if (File.separatorChar == '\\')// windows
            path += '\\' + text;
        else
            path += "//" + text;
        return path;
    }
}
