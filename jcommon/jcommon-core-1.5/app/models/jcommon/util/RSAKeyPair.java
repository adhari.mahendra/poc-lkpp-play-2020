/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package models.jcommon.util;

/**
 *
 * @author sgp & kongja
 */
import java.math.BigInteger;
import java.security.SecureRandom;

/** An RSA key pair.
* This class produces an RSA key pair with a 3072 bit modulus and a public exponent of 17.*/
/* kongja modified public exponent of 65537*/
public final class RSAKeyPair{
	
	/** The value 17, used as a common public expoonent in the whisper protocol.*/
	//public static final BigInteger E17=BigInteger.valueOf(17);
	public static final BigInteger E17=BigInteger.valueOf(65537);// Modifikasi
        
	private static final int SIZE=1024;
	private static final int CERTAINTY=80;
	private static final int HALF=512; // Half the key size
	
	private BigInteger P;
	private BigInteger Q;
	private BigInteger N;
	private BigInteger D;
	private BigInteger DP;
	private BigInteger DQ;
	private BigInteger QInvP;
	
	/** Generate a new RSA keypair.
	* This will take a long time to execute.
	* @param sr A secure random source for generating the key.*/
	public RSAKeyPair(SecureRandom sr){
		Thread t=Thread.currentThread();
		boolean doneD=false;
		while(!doneD){
			
		P=new BigInteger(HALF,CERTAINTY,sr);
		if(t.isInterrupted()){
			return;
		}
		Q=new BigInteger(HALF,CERTAINTY,sr);
		if(t.isInterrupted()){
			return;
		}
		N=P.multiply(Q);        
		while (N.bitLength()!=SIZE){
			if(t.isInterrupted()){
				return;
			}
			if(P.compareTo(Q)==-1){
				P=Q;
			}
			Q=new BigInteger(HALF,CERTAINTY,sr);
      
			N=P.multiply(Q);           
		}
		BigInteger p_1=P.subtract(BigInteger.ONE);
		BigInteger q_1=Q.subtract(BigInteger.ONE);
		BigInteger p_1q_1=p_1.multiply(q_1);
		
		
			try{
				D=E17.modInverse(p_1q_1);
				doneD=true;
				DP = D.remainder(p_1);
			DQ= D.remainder(q_1);
			QInvP=Q.modInverse(P);
			}
			catch (ArithmeticException ae){
				doneD=false;
			}
		}
		
	}
	
	/** @return The private prime value p.*/
	public BigInteger getP(){
		return P;
	}
	
	/** @return The private prime value q.*/
	public BigInteger getQ(){
		return Q;
	}
	
	/** @return The public value n.
	* N=P*Q*/
	public BigInteger getN(){
		return N;
	}
	
	/** @return The public exponent value e.*/
	public BigInteger getE(){
		return E17;
	}
	
	/** @return The private exponent value d.*/
	public BigInteger getD(){
		return D;
	}
	
	/** @return The private value dp.
	* DP= remainder of D divided by (P-1)*/
	public BigInteger getDP(){
		return DP;
	}
	
	/** @return The private value dq.
	* DQ= remainder of D divided by (Q-1)*/
	public BigInteger getDQ(){
		return DQ;
	}
	
	/** @return The private value qinvp.
	* QInvP=Q^(-1) mod P*/
	public BigInteger getQInvP(){
		return QInvP;
	}
	
	/** Wipes the internal data of the keypair.
	* Note if the data is represented elsewhere, such as a PrivateKey object,
	* the data will still exist.*/
	public void wipe(){
		P=null;
		Q=null;
		N=null;
		D=null;
		DP=null;
		DQ=null;
		QInvP=null;
	}
}