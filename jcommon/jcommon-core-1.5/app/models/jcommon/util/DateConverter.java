package models.jcommon.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter implements JsonDeserializer<Date> {

    DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ");
    DateFormat df2 = new SimpleDateFormat("MMM dd,yyyy");

    @Override
    public Date deserialize(final JsonElement json, final Type typeOfT, final JsonDeserializationContext context)
            throws JsonParseException {
        try {
            return df.parse(json.getAsString());
        } catch (ParseException e) {
            try {
                return df2.parse(json.getAsString());
            } catch (ParseException ex) {
                return null;
            }
        }
    }

}
