package models.jcommon.util;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;

import play.Logger;
import play.Play;

/**TempFileManager ini akan memastikan bahwa file-file akan dihapus ketika
 * 1. JVM terminate (lewat file.deleteOnExit())
 * 2. Jika #1 tidak bisa, maka saat startup akan ada penghapusan file-file yg sudah expires 
 * 	(definisi expires yaitu, yg di-create > 24 jam yang lalu) 
 * @author andik
 *
 */

public class TempFileManager {

	
	
	private static String sufix;
	public final static String customTempFolder; //base folder ini berbeda dengan play.tmp di application.conf
	public static int TEMP_FILE_AGE;
	static
	{
		/* sufix sesuai dengan application path untuk memastikan agar tidak
		 * men-delete file yg di-generate oleh app Play! yang lain.
		 *  
		 */
		sufix=Integer.toHexString(Play.applicationPath.hashCode());
		String sysTemp=System.getProperty("java.io.tmpdir");
		if(!sysTemp.endsWith(File.separator))
			sysTemp=sysTemp+File.separator;
		customTempFolder= sysTemp+ "playapplkpp." + sufix;
		File file=new File(customTempFolder);
		file.mkdirs();
		
		TEMP_FILE_AGE=Integer.parseInt(Play.configuration.getProperty("temp.file.age", "24"));
	}
	
	/**
	 * Hapus semua file yang memiliki sufix tertentu di folder
	 * 1. System Temp 
	 * 2. Play Temp > Play.applicationPath/tmp/uploads
	 * 3. Play Temp > Play.applicationPath/tmp 
	 */
	public static void deleteAllTempFiles()
	{
		
		try {
			//1. Play Temp > Play.applicationPath/tmp
			FileUtils.deleteDirectory(new File(customTempFolder));
			
			//2. Delete Play temporay files: "${play.tmp}"
			File file=Play.tmpDir;
			String path=file.toString();
			final long hours24Ago=DateUtils.addHours(new Date(), TEMP_FILE_AGE).getTime();
			file.listFiles(new FileFilter() {
				public boolean accept(File pathname) {
					if(pathname.isFile())
						try
						{
							if(pathname.lastModified()<hours24Ago);
								pathname.delete();
						}
						catch(Exception e)
						{};
					return true;
				}
			});
			
			//2. Delete Play temporary folder ${play.tmp}/uploads
			path=path + File.separatorChar + "uploads";
//			Logger.debug("Play! Temporary folder deleted: %s", path);
			FileUtils.deleteQuietly(new File(path));
			
			//3.  Play Temp > ${play.tmp}/* >  
			/* folder tsb digunakan untuk play untuk buffer ketika submit <input type=file> 
			 */
			File playTempRoot=Play.tmpDir;
			final long current=System.currentTimeMillis();
			final long AGE=1000*60*10; //10menit
			playTempRoot.listFiles(new FileFilter() {				
				public boolean accept(File pathname) {
					//jika di-create lebih dari sekian menit yang lalu maka di-delete
					if(current - pathname.lastModified()>=AGE)
						pathname.delete();
					return false;
				}
			});
			
		} catch (IOException e) {
			Logger.error("ERROR Deleting temporary file in: %s" , customTempFolder);
		}
		Logger.debug("Custom TempFile deleted: %s'", customTempFolder);
	}
	
	/**Create temporary file
	 *  
	 * @return
	 * @throws IOException
	 */
	public static File createTemporaryFile(String prefix) throws IOException
	{
		File file=File.createTempFile(prefix, sufix);
		//delete on exit. But it is quaranted to work
		file.deleteOnExit();
		return file;
	}
	
	
	/**Open file created in temporary folder */
	public static File openFile(String fileName)
	{
	
		File file=new File(customTempFolder + "/" + fileName);
		return file;
	}
	
	public static File createFileInTemporaryFolder(String fileName) throws IOException
	{
		return createFileInTemporaryFolder(fileName, true);
	}
	/**Create file with specific filename, in temporary folder
	 * @param fileName filename to create
	 * @param force if true, file will we deleted if already exists, 
	 * 			if false, error occurs if file arleady exists
	 * @return
	 * @throws Exception 
	 */
	public static File createFileInTemporaryFolder(String fileName, boolean force) throws IOException
	{
		File file=new File(customTempFolder + "/" + fileName);
		if(file.exists())
			if(force)
				file.delete();
			else
				throw new IOException("File already exists: " + file.toString());
		try
		{
			file.getParentFile().mkdirs();
			file.createNewFile();
		}
		catch(IOException e)
		{
			throw new IOException(file.toString() + " Error: " + e.toString());
		}
		//delete on exit. But it is quaranted to work
		file.deleteOnExit();
		return file;
	}
	
}
