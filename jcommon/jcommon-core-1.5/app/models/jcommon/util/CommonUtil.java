package models.jcommon.util;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import com.google.gson.*;

public class CommonUtil {

	public static final Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateConverter()).registerTypeAdapter(Double.class, new DoubleConverter()).create();
	
	/**
	 * fungsi pengecekan data yang berupa Array<E>
	 * @param array
	 * @author Arief Ardiyansah
	 */
	public static boolean isEmpty(final Object[] array) {
		return ((array == null) || (array.length == 0));
	}
	
	/**
	 * fungsi pengecekan data yang berupa java.util.Collection<E>
	 * @param collection
	 * @author Arief Ardiyansah
	 */
	public static boolean isEmpty(final Collection<?> collection) {
		return (collection == null || collection.isEmpty());
	}

	/**
	 * fungsi pengecekan data yang berupa java.util.Map<K, V>
	 * @param map
	 * @author Arief Ardiyansah
	 */
	public static boolean isEmpty(final Map<?,?> map) {
		return ((map == null) || (map.isEmpty()));
	}	
	
	/**
	 * fungsi pengecekan data yang berupa java.lang.String<E>
	 * @param value
	 * @author Arief Ardiyansah
	 */
	public static boolean isEmpty(final String value) {
		return ((value == null) || (value.isEmpty()));
	}
	
	/**
	 * convert Json to Object
	 * @param json
	 * @param classOfT
	 * @return
	 */
	public static <T> T fromJson(final String json, Class<T> classOfT) {
		return gson.fromJson(json, classOfT);
	}	
	
	/**
	 * convert Json to Object
	 * @param json
	 * @param typeOfT
	 * @return
	 */
	public static <T> T fromJson(final String json, Type typeOfT) throws JsonIOException, JsonSyntaxException {
		return gson.fromJson(json, typeOfT);
	}

	public static <T> T fromJson(final JsonElement json, Class<T> classOfT) {
		return gson.fromJson(json, classOfT);
	}
	
	/**
	 * convert Object to Json
	 * @param src
	 * @return
	 */
	public static String toJson(final Object src) {
		if (src == null) {
			return toJson(JsonNull.INSTANCE);
		}
		return gson.toJson(src, src.getClass());
	}
	
	public String toJson(final Object src, Type typeOfSrc) {
		return gson.toJson(src, typeOfSrc);
	}

	public static JsonElement toJsonTree(Object src) {
		return gson.toJsonTree(src);
	}
}
