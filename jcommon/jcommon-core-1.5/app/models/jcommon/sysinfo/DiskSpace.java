package models.jcommon.sysinfo;

import java.io.File;

public class DiskSpace {
	
	public Long freeSpace;
	public Long totalSpace;
	public Long percentFree;
	private File file;
	
	//get disk space information
	public DiskSpace(File file)
	{
		freeSpace=file.getFreeSpace();
		totalSpace=file.getTotalSpace();
        if(totalSpace != null && totalSpace > 0)
		    percentFree=freeSpace*100/totalSpace;
		this.file=file;
	}
	
	public String toString()
	{
		String str=file.toString().replace('\\', '/');
		int pos=str.indexOf((int)'/', 4);
		if(pos>0)
			str=str.substring(0, pos) + "...";
//		return null;
		return String.format("%s: %s of %s (%s%%)", str, ext.FormatUtils.formatBytes(freeSpace), ext.FormatUtils.formatBytes(totalSpace), percentFree);
	}
}
