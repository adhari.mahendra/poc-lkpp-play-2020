
package models.jcommon.db.base;
import java.beans.PropertyVetoException;
import java.io.ByteArrayOutputStream;
import java.io.Writer;
import java.sql.*;
import java.util.Date;
import java.util.zip.GZIPOutputStream;

import play.Play;
import play.db.DB;
import play.db.jdbc.Query;



/**Class ini digunakan untuk akses ke JDBC secara langsung */
public class JdbcUtil {

//	static HikariDataSource dataSource=null;
	
	/**Eksekusi SQL menggunakan JDBC 
	 * @throws PropertyVetoException 
	 * @throws SQLException */
	public synchronized static long executeUpdateUsingJDBC(String sql, Object... params) 
	{
		long result=-1;
	
		Connection conn=null;
		try {
			conn = getConnection();

		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		PreparedStatement st=null;
		try {
			conn.setAutoCommit(true);
			st=conn.prepareStatement(sql);
			setParams(st, params);
			result=st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			try
			{
				st.close();
				
			}
			catch(SQLException e)
			{
				e.printStackTrace();
			}			
		}		
		return result;
	}
	
	/**Execute Query Statement using JDBC */
	public static ResultSet executeQueryUsingJDBC(String sql, Object... params) 
	{
	
		Connection conn=null;
		try {
			conn = getConnection();
			if(conn.isClosed())
				getConnection();
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		PreparedStatement st=null;
		try {
			conn.setAutoCommit(true);
			st=conn.prepareStatement(sql);
			setParams(st, params);
			return st.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally
		{
			
		}
		return null;
	}

	private static void setParams(PreparedStatement st, Object...  params) throws SQLException {
		if(params!=null)
		{
			int size=params.length;
			for(int i=1;i<=size;i++)
			{
				Object param=params[i-1];
					if(param==null)
					st.setObject(i,null);
				else
				{
					//jika enum maka konvert jadi String				
					if(param.getClass().isEnum())
						param=param.toString();					
					if(param instanceof String)
						st.setString(i, (String)param);
					else if(param instanceof Long)
						st.setLong(i, (Long)param);
					else if(param instanceof Integer)
						st.setInt(i, (Integer)param);	
					else if(param instanceof Date)
						st.setObject(i, param, Types.TIMESTAMP);	
					else
						st.setObject(i, param);		
				}
			}
		}
		
	}

	/*private static void getDataSource() {
		if(dataSource==null)
		{
			dataSource=new HikariDataSource();
			String url=Play.configuration.getProperty("db.url");
			String user=Play.configuration.getProperty("db.user");
			String password=Play.configuration.getProperty("db.pass");
			String driver=Play.configuration.getProperty("db.driver");
			String poolTimeOut=Play.configuration.getProperty("db.pool.timeout", "1000");
			String poolMaxSize=Play.configuration.getProperty("db.pool.maxSize", "30");
			String poolMinSize=Play.configuration.getProperty("db.pool.minSize", "10");
			String poolIdleTime=Play.configuration.getProperty("db.pool.maxIdleTime", "10000");
//			try {
				Logger.info("Direct JDBC Connection Created: %s", url);
				
				dataSource.setDriverClassName(driver);
				 //loads the jdbc driver
				dataSource.setJdbcUrl(url); 
				dataSource.setUsername(user);
				dataSource.setPassword(password); // the settings below are optional -- c3p0 can work with defaults

				dataSource.setMinPoolSize(Integer.parseInt(poolMinSize)); 
				dataSource.setAcquireIncrement(5); 
//				dataSource.setMaximumPoolSize(Integer.parseInt(poolMaxSize));
				dataSource.setMaxIdleTime(Integer.parseInt(poolIdleTime));
				dataSource.setCheckoutTimeout(Integer.parseInt(poolTimeOut));
				
				//TODO connection poll masih kacau, tidak bisa getConnection
				Harus dikonfigurasi idle timeout dll 
				 * 
				 
			} catch (PropertyVetoException e) {
				e.printStackTrace();
			}
		};
		
		
	}*/

	/**Get connection using direct JDBC. AutoCommit=true
	 * 
	 * @return
	 * @throws SQLException
	 */
	private static Connection conn=null;
	public static Connection getConnection() throws SQLException {
		
		if(conn==null)
		{
//			getDataSource();
			conn= DB.getConnection();
		}
		else
			if(conn.isClosed())
			{
//				getDataSource();
				conn= DB.getConnection();
			}
		return conn;
	}
	
	
	/**Get without connection poll 
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnectionWithoutPool() throws SQLException {
		String url=Play.configuration.getProperty("db.url");
		String user=Play.configuration.getProperty("db.user");
		String password=Play.configuration.getProperty("db.pass");
		Connection conn=DriverManager.getConnection(url, user, password);
		return conn;
	}
	
	public static void close(Statement stmt, ResultSet rs) {
	      close(stmt);
	      close(rs);
	   }

	
	public static void close(Connection con) {
//		if (con != null) {
//			try {
//				//con.close();
//			} catch (SQLException ex) {
//				//
//			} catch (Throwable ex) {
//				//
//			}
//		}
	}

	
	public static void close(Statement stmt) {
		if (stmt != null) {
			try {
				stmt.close();
			} catch (SQLException ex) {
				//
			} catch (Throwable ex) {
				//
			}
		}
	}

	public static void close(ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException ex) {
				//
			} catch (Throwable ex) {
				//
			}
		}
	}
	
	/**
	 * 
	 * count di postgresql sangat lambat jika data sudah mencapai > 1M
	 * https://wiki.postgresql.org/wiki/Slow_Counting
	 */
	public static long count_estimate(String sql, Object... params) {		
		if(sql.toLowerCase().contains("where")) { // jika ada WHERE maka execute SQL 
			return Query.count(sql, params);	
		} else {
			final String result = (String)Query.find("EXPLAIN " +sql, String.class, params).fetch().get(1);
			final String row = result.substring(result.lastIndexOf("rows=")+5,result.lastIndexOf(" "));
			return Long.valueOf(row);		
		}			
	}

	static final String CSV_LINE_SEPARATOR = "|";

	// resultset query to csv formatted gzipped
	public static String resultSetToCsv(String sql) {
		// Ensure line separator correct for our platform
		StringBuilder result = new StringBuilder();
		try {
			Statement stmt = DB.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			ResultSetMetaData metaData = rs.getMetaData();
			int numberOfColumns = metaData.getColumnCount();
			for (int column = 0; column < numberOfColumns; column++) {
				result.append(metaData.getColumnLabel(column + 1));
				result.append(CSV_LINE_SEPARATOR);
			}
			result.append("\r\n");
			// Get all rows.
			while (rs.next()) {
				for (int i = 1; i <= numberOfColumns; i++) {
					result.append(rs.getObject(i));
					result.append(CSV_LINE_SEPARATOR);
				}
				result.append("\r\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result.toString();
	}
}
