package models.jcommon.db.base;

import java.io.Serializable;
import java.util.Date;
import java.util.Random;

import javax.persistence.MappedSuperclass;

import org.apache.commons.codec.binary.Base64;

import ext.FormatUtils;
import models.jcommon.util.DateUtil;
import models.jcommon.util.TeaCipher;
import play.db.jdbc.BaseTable;
import play.mvc.Scope.Session;

/**
 * BaseDomainObject
 * This class is the base class for all model; compatible with SPSE 3.x
 * SEBELUMNYA ada di sso.common dan dipindah ke sini oleh AY agar konsisten
 *
 */

@MappedSuperclass
public class BaseDomainObject extends BaseTable implements Serializable {

	private static TeaCipher cipher;

	public String audittype;

	public String audituser;

	public Date auditupdate;

	static {
		Random rand = new Random();
		byte[] key = new byte[16];
		rand.nextBytes(key);
		cipher = new TeaCipher(key);
	}

	/**
	 * Encrypt a long number into String. Encryption should be fast
	 */
	public static String encryptId(long id) {
		return encrypt(id);
	}

	/**
	 * Decrypt an encrypted String to a long value
	 *
	 * @param id encrypted string
	 * @return long value
	 */
	public static long decryptId(String id) {
		return decrypt(id);
	}

	public String getFormattedAudiupdate() {
		return FormatUtils.formatDateTimeInd(auditupdate);
	}

	private static String encrypt(long id) {
		// 1. convert to hex
		String st = Long.toHexString(id);
		// 2. get byte[] an encrypt
		byte[] result = cipher.encrypt(st.getBytes());
		// 3. convert to base64String
		st = Base64.encodeBase64String(result);
		return st;
	}

	private static long decrypt(String id) throws RuntimeException {
		try {
			// 1: decodeBase64
			byte[] result = Base64.decodeBase64(id);
			// 2: decrypt
			result = cipher.decrypt(result);
			String st = new String(result);
			// 3: convert from hex to dec
			return Long.parseLong(st, 16);
		} catch (Exception e) {
			throw new RuntimeException("Akses ditolak karena URL telah mengalami perubahan.");
		}
	}

	/**
	 * Fungsi {@code setUp} dijalankan ketika sebuah model akan dibuat ataupun di-update.
	 * Fungsi ini melakukan setting terhadap kolom audittype, audituser dan auditupdate.
	 */
	public void prePersist() {
		audittype = audittype != null && !audittype.isEmpty() ? "U" : "C";
		if(audituser == null && !audittype.isEmpty()) { // AA : diset hanya jika audituser == null, karena bisa jadi diset oleh aplikasi yang pakai ini
			Session current_session = Session.current();
			if (current_session != null) {
				/**REstore by Andik karena ActiveUser tidak dikenal di jcommon (ActiveUser adalah model
				 * di SPSE 4. jcommon harus terbebas dari dependensi ke app utama)
				 *
				 *
				 */
				audituser = current_session.get(BaseModel.SESSION_USERID); // set audituser, commented by irvan

				if (audituser == null)
					audituser = "N/A";
			} else { // handling jika session object tidak ada, misal saat mengerjakan JOB
				audituser = "PLAY-AUTO-JOBS";
			}
		}
		auditupdate = DateUtil.newDate();
	}
}
