package models.jcommon.db.exporter;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.apache.commons.lang.time.StopWatch;

import models.jcommon.db.base.JdbcUtil;
import models.jcommon.util.TempFileManager;
import play.Logger;

@Deprecated
public class JsonExporter{
	

	public enum FILE_FORMAT {
		BZIP2, TEXT
	}

	//versi dari JsonExporter
	public static final String VERSION="1.0.0";
	
	private OutputStream outResultSet;
	
	
	/** Data hasil query, @see models.jcommon.db.exporter.JsonExporter.getJsonAsFile(String, boolean)
	 * 
	 */
	private File fileResultSet;
	
	/* data MD5 dari ResultSet
	 * Format sbb (PENTING: Newline DIGUNAKAN UNTUK PARSING)
	 * [
	 * 	[MD5Total]	
	 * 	,[MD5Row, pk1, pk2, ...]
	 * 	,[MD5Row, pk1, pk2, ...]
	 * ]
	 * 
	 * Jika tidak ada row maka menjadi
	 * [
	 * 	[MD5Total]
	 * ]
	 */
	private File fileMD5Info=null;
	private OutputStream outMD5;
	
	//Md5Total untuk seluruh row
	public String md5Total;
	
	private int colCount;
	
	/* ini adalah index dari kolom PK (selalu diawali tanda koma). 
	 * Digunakan ketika generate MD5Row
	 * 	Contoh: ,0
	 * 			,0,1  
	 * 
	 */
	private String primaryKeyColoumnsIndex;
	
	/**Constructor ini digunakan di sisi server di mana 
	 * 1. perlu output MD5 saja
	 * 2. tidak perlu output Json untuk datanya
	 * 
	 * @param uniqueId
	 * @throws IOException
	 * @throws CompressorException
	 */
	public JsonExporter(String uniqueId) throws IOException
	{
		this(null, uniqueId);
	}
	
	public JsonExporter(FILE_FORMAT format) throws IOException
	{
		this(format, null);
	}
	
	/**Export menjadi file dengan format bzip2 */
	public JsonExporter(FILE_FORMAT format, String uniqueId) throws IOException
	{	
		if(uniqueId==null)
			uniqueId=UUID.randomUUID().toString();
		if(format==FILE_FORMAT.BZIP2) 
		{
			Logger.warn("BZIP2 JsonExporter not implemented yet"); 
			fileResultSet=TempFileManager.createFileInTemporaryFolder(uniqueId + JsonExporterReader.JSON_BZIP2);
			outResultSet = new FileOutputStream(fileResultSet);
//			CompressorOutputStream cos = new CompressorStreamFactory().createCompressorOutputStream("bzip2", outResultSet);
//			outResultSet=cos;
		}
		else
		if(format==FILE_FORMAT.TEXT)
		{
			fileResultSet=TempFileManager.createFileInTemporaryFolder(uniqueId + JsonExporterReader.JSON_TEXT);
			outResultSet=new BufferedOutputStream(new FileOutputStream(fileResultSet), 1024 * 128);
		}
		else
		//
		{
			fileResultSet=null;
			outResultSet=new NullOutputStream();
		}
		
		fileMD5Info=TempFileManager.createFileInTemporaryFolder(uniqueId + JsonExporterReader.MD5);
		outMD5=new FileOutputStream(fileMD5Info);
	}

	
	
	public File getJsonAsFile(String query) throws SQLException, IOException, NoSuchAlgorithmException {
		return getJsonAsFile(query, false, null);
	}
	
	/**Format of json return
	 * row 0: header
	 * row 1: column names
	 * row 2..n: contents
	 * 		header: [VERSION, STATUS]
	 * 		STATUS: OK|ERROR:.....|....
	 * 
	 * @param primaryKey String dalam format Json berisi nama-nama kolom PK. Contoh ["agc_id"]
	 * @throws SQLException
	 *  
	 * @throws IOException 
	 * @throws NoSuchAlgorithmException 
	 * */
	/* Sample Statistics (rekanan)
	 * Using PrintStream: 180MB file  in 10 minutes in PC, 466.000 rows
	 * Using BufferedOutputStream: 1 minutes 
	 * 
	 */
	public File getJsonAsFile(String query, boolean withMD5Column, String primaryKey) throws SQLException, IOException, NoSuchAlgorithmException {
	
		if(primaryKey==null)
			throw new IllegalArgumentException("primaryKey cannot be null");
		ResultSet rs =null;
		String status="OK";
		Connection conn=null;;
		Statement st=null;
		StopWatch sw=new StopWatch();
		sw.start();
		try
		{
			conn=JdbcUtil.getConnection();
			//urut berdasarkan PK
			String orderByPK=primaryKey.replaceAll("\"|\\[|\\]", "");
			query=query + " order by " + orderByPK + " desc";
			//data may by large, so we must limit fetch size
			st=conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			st.setFetchDirection(ResultSet.FETCH_FORWARD);
			//without this, all data is fetch into memory
			st.setFetchSize(1000);
			rs=st.executeQuery(query);
			Logger.debug("SQL: %s", query);
			convertPrimaryKeyIntoColumnIndex(rs, primaryKey);
			
		}
		catch(Exception e)
		{
			status="ERROR: " + e.toString();
			Logger.error("Error executing query %s: %s", query, e.toString());
		}
		
		writeHeader(rs, status, withMD5Column);
		int rowCount=0;
		if(rs!=null)
			rowCount=writeContent(rs, withMD5Column);	
		if(st!=null)
			st.close();
		if(conn!=null)
			conn.close();
		
		//closin. 
		print("\n]");
				
		outResultSet.flush();
		outResultSet.close();
		sw.stop();
		String size="";
		if(fileResultSet==null)
			size="Not Generated";
		else
			size=String.valueOf(fileResultSet.length());
		Logger.debug("[JSON] [DONE] DB.writeJson. Rows: %,d, duration: %s, size: %,d", rowCount, sw.toString(), size);
		
		return fileResultSet;
	}
	
	private void convertPrimaryKeyIntoColumnIndex(ResultSet rs,
			String primaryKey) throws SQLException {
		ResultSetMetaData md=rs.getMetaData();
		primaryKey=primaryKey.toLowerCase();
		StringBuilder str=new StringBuilder();
		for(int i=1;i<=md.getColumnCount();i++)
		{
			String colName=md.getColumnName(i).toLowerCase();
			if(primaryKey.contains("\"" + colName + "\""))
			{
				str.append(',');
				str.append(i);
			}
		}
		
		primaryKeyColoumnsIndex=str.toString();
	}

	//Print st to outStream
	private void print(String st) throws IOException
	{
		outResultSet.write(st.getBytes());
	}
	
	private void print(char ch) throws IOException
	{
		outResultSet.write(ch);
	}

	private int writeContent(ResultSet rs, boolean withMD5Column) throws SQLException, IOException, NoSuchAlgorithmException {
		int rowCount=0;
		
		/* MD5 per rows
		 * dg format 
		 * 	[MD5, pk1, pk2, ...],
		 * 	[MD5, pk1, pk2,...]
		*/
		StringBuilder md5PerRowsAndPK=new StringBuilder(1000);
		MessageDigest digest = MessageDigest.getInstance("MD5");
		while(rs.next())
		{
			//row disimpan di stringbuilder, setelah terbentuk baru satu row di.print ke out
			print(",[");
			StringBuilder row=new StringBuilder(10000);
			StringBuilder pkInfo=new StringBuilder(100);
			for(int i=1; i<=colCount;i++)
			{
				if(i>=2)
					row.append(',');
				String data=rs.getString(i);
				StringBuilder colValue= withEscapeCharacters(data);
				row.append(colValue.toString());
				String pkIndex="," + (i) ;
				if(withMD5Column)
					if(primaryKeyColoumnsIndex.contains(pkIndex))
					{
						if(pkInfo.length() > 0)
							pkInfo.append(',');
						pkInfo.append(colValue);
					}
			}	
			
			//add mdd5
			if(withMD5Column)
			{
				String rowValue=row.toString();
				String md5Value=DigestUtils.md5Hex(rowValue);
				//tambahkan md5 di kolom terakhir
				row.append(",\"");
				row.append(md5Value);
				row.append("\"");
				
				//Info MD5Row
				md5PerRowsAndPK.append("\n,[\"");
				md5PerRowsAndPK.append(md5Value);
				md5PerRowsAndPK.append("\",");
				
				//PK
				md5PerRowsAndPK.append(pkInfo);				
				md5PerRowsAndPK.append(']');
				
				//MD5 for all md5Value
				digest.update(md5Value.getBytes());
			}
			print(row.toString());
			print("]\n");
			outResultSet.flush();
			rowCount++;
		}
		
		byte[] digestValue=digest.digest();
		if(withMD5Column)
		{
			//new line berlaku, TIDAK BOLEH DIABAIKAN
			outMD5.write("[[\"".getBytes());
			md5Total = new String(Hex.encodeHex(digestValue));
			outMD5.write(md5Total.getBytes());
			outMD5.write("\"]\n".getBytes());
			outMD5.write(md5PerRowsAndPK.toString().getBytes());
			outMD5.write("\n]".getBytes()); //Harus ditambahkan newline
			outMD5.flush();
			outMD5.close();
		}
		
		return rowCount;
	}

	
	private void writeHeader(ResultSet rs, String status, boolean withMD5Column) throws SQLException, IOException {
		print("[[");
		print(withEscapeCharacters(VERSION).toString());
		print(',');
		print(withEscapeCharacters(status).toString());
		print("]\n");
		if(rs!=null)	//metadata
		{	
			ResultSetMetaData rsMD=rs.getMetaData();
			colCount=rsMD.getColumnCount();
			print(",[");		
			for(int i=1; i<= colCount; i++)
			{
				if(i>1)
					print(',');
				StringBuilder str=withEscapeCharacters(rsMD.getColumnName(i));
				print(str.toString());
			}
			//add MD5column
			if(withMD5Column)
			{
				print(',');
				print("\"*md5\"");
			}
			print("]\n");
		}
	}
	
	/**Cetak dengan escape char
	 * \ diganti \\
	 * NEW_LINE diganti \n
	 * CR	diganti \r 
	 * @throws IOException 
	 */
	private StringBuilder withEscapeCharacters(String data) throws IOException
	{
		StringBuilder str=new StringBuilder();
		if(data==null)
			str.append("null");
		else
		{//add escape char
			str.append("\"");
			data=data.replaceAll("\"", "\\\"");
			data=data.replaceAll("\n", "\\\\n");
			data=data.replaceAll("\r", "\\\\r");			
			str.append(data);
			str.append("\"");
		}
		return str;
	}

	public File getMD5File() {
		return fileMD5Info;
	}
}
