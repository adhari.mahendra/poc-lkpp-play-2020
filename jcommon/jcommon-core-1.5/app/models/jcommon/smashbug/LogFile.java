/**
 * 
 */
package models.jcommon.smashbug;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.StopWatch;

import play.Logger;
import play.Play;

/**
 * @author AndikYulianto@yahoo.com
 *
 */
public class LogFile {

	public String findLogMessage(String id)
	{
		StopWatch sw=new StopWatch();
		sw.start();
		File logFolder=new File(Play.applicationPath + "/logs");
		//urutkan berdasarkan last modif
		Set<File> files=new TreeSet<>(new Comparator<File>() {
			public int compare(File o1, File o2) {
				if (o1.lastModified() < o2.lastModified())
					return 1;
				return -1;
			}
		});
		files.addAll(Arrays.asList(logFolder.listFiles()));
		if(!id.startsWith("@"))
			id="@" + id;
		int fileScan=1;
		String msg=null;
		long fileSize=0;
		for(File file: files)
		{
			 msg=findInFile(file, id);
			 Logger.debug("Searching in file: %s of %s", file,files.size());
			 fileSize+=file.length();
			if(msg.length()>0)
				break;
			fileScan++;
		}
		sw.stop();
		Logger.debug("Find text %s in log files, files scanned: %s (%s), duration: %s", id, fileScan, FileUtils.byteCountToDisplaySize(fileSize), sw);
		if(msg.length()==0)
			msg=null;
		return msg;
	}

	private String findInFile(File file, String id) {
		
		CircularFifoQueue<String> previousString=new CircularFifoQueue<>(10); //simpan beberapa baris sebelumnya
		try (LineNumberReader reader=new LineNumberReader(new BufferedReader(new FileReader(file), 1024*100))){
			String st=null;
			boolean startIsFound=false;
			StringBuilder strResult=new StringBuilder(1024*10);
			while((st=reader.readLine())!=null)
			{
				if(startIsFound)
				{
					strResult.append(st).append('\n');
					if(st.startsWith("\t... "))
					{
						//tambahkan beberapa baris sebelumnya ke dalam result
						for(String stPrev: previousString)
							strResult.insert(0, stPrev).insert(0, '\n');
						break;
					}
				}
				else
					previousString.add(st);
//				Logger.debug("[LOG %4d] %s", line++,st);
				if(st.startsWith(id))
				{
					strResult.append("\n===================================================================================================\n").append(st).append('\n');
					startIsFound=true;
				}
			}
			
			reader.close();
			return strResult.toString();
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
