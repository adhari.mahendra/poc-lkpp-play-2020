package controllers.jcommon.blob;

import models.jcommon.blob.BlobTable;

/**
 * Kelas {@code DefaultDownloadHandler} digunakan untuk handling download File yang tidak memerlukans security khusus
 *
 * Created by IntelliJ IDEA.
 * Date: 12/12/12
 * Time: 17:35
 * @author I Wayan Wiprayoga W
 */
public class DefaultDownloadHandler implements DownloadSecurityHandler {
	/**
	 * TODO: Seharusnya semua yang boleh download file didefinisikan disini
	 * @param secureIdBlobTable objek {@link BlobTable}
	 * @return nilai true memperbolehkan download
	 */
	@Override
	public boolean allowDownload(BlobTable secureIdBlobTable) {
		return true;
	}
}
