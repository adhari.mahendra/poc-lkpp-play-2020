package jobs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Date;

import models.jcommon.blob.BlobTable;
import models.jcommon.blob.BlobTableDao;
import models.jcommon.blob.BlobTableMigration;

import org.apache.commons.lang.time.StopWatch;

import play.Logger;
import play.Play;
import play.jobs.Job;

/** Job ini bertugas untuk migrasi BlobTable versi 3.5 ke versi 4.0
 * Job ini dijalankan pada jam 23:00 hari jumat
 *  */
//@Every("0 0 23 ? * FRI *")
//Enable jika sudah siap untuk produksi. Pada development, disable selalu 
//@OnApplicationStart(async=true)
public class FileMigrationJob extends Job<Boolean> {
	
	public void doJob()
	{
		Logger.debug("[STARTED] MigrationJob started. ");
		StopWatch sw=new StopWatch();
		sw.start();
		int failedCount=0;
		int count=0;
		try {			
			String sql="blb_engine is null order by blb_id_content, blb_versi ";
			
			//create migrationLog (& folders if necessary)
			File fileLog=new File(Play.applicationPath+"/logs/file.migration.log");
			fileLog.getParentFile().mkdirs();
			fileLog.createNewFile();
			FileOutputStream log= new FileOutputStream(fileLog, true);
			PrintStream out=new PrintStream(log);
			out.format("***** Migration started at %s ********** \n", new Date());
			while(true)
			{
				BlobTable blob=BlobTableDao.find(sql).first();
				if(blob==null)
					break; 
				if(!BlobTableMigration.migrateSingleFile(blob, out))
					failedCount++;
				else
					count++;
				if(failedCount>=10)
				{
					Logger.error("[STOPPED] Migration process has been stopped because %s errors occcurs", failedCount);
					break;
				}
			}
			out.close();
		} catch (Exception e) {			
			e.printStackTrace();
		}
		sw.stop();
		Logger.debug("[DONE] MigrationJob done. %s files migrated in %s ", count, sw.toString());
	}
}
;