//convert ke array buffer
function s2ab(s) {
    if(typeof ArrayBuffer !== 'undefined') {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    } else {
        var buf = new Array(s.length);
        for (var i=0; i!=s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }
}
//create-workbook
function generateXlsxUser() {

    var ws_name = "USER";

    var wscols = [
        {wch:4},
        {wch:15},
        {wch:15},
        {wch:25},        
        {wch:25},        
        {wch:15}

    ];

    var wsrows = [
        {hpt: 17},
        {hpt: 47},
    ];

    var ws_data = [["Report User"],["No","Nama", "Username", "Email","NPWP","Role"]];

    for(var i = 0; i < data_user.length; i++){
        var model = [ i + 1, 
            // data_user[i]['id'], 
            data_user[i]['nama'], 
            data_user[i]['username'], 
            data_user[i]['email'], 
            data_user[i]['npwp'],
            data_user[i]['role_id']];
        ws_data.push(model);
    }

    var wb = XLSX.utils.book_new();

    var ws = XLSX.utils.aoa_to_sheet(ws_data);

    ws['!merges'] = [{ s: 'A1', e: 'E1' }];

    ws['!cols'] = wscols;

    //ws['!rows'] = wsrows;

    //ws['!autofilter'] = { ref: "A4:E4" };

    XLSX.utils.book_append_sheet(wb, ws, ws_name);

    var wopts = { bookType:'xlsx', bookSST:false, type:'binary' };

    var wbout = XLSX.write(wb,wopts);

    var filename = "User.xlsx";

    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), filename);
}

function generateJsonUser(){
    saveAs(new Blob([JSON.stringify(data_user)],{type:"application/json"}), "User.json");
}
