//convert ke array buffer
function s2ab(s) {
    if(typeof ArrayBuffer !== 'undefined') {
        var buf = new ArrayBuffer(s.length);
        var view = new Uint8Array(buf);
        for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    } else {
        var buf = new Array(s.length);
        for (var i=0; i!=s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
        return buf;
    }
}
//create-workbook
function generateXlsx() {

    var ws_name = "TRANSAKSI";

    var wscols = [
        {wch:25},
        {wch:25}
    ];

    

    var wsrows = [
        {hpt: 17},
        {hpt: 47},
    ];

    var ws_data = [["Report Transaksi"],["No","Nama Barang", "Jumlah", "Harga Satuan","Total", "Status"]];

    for(var i = 0; i < data_transaksi.length; i++){
        var model = [ i + 1, 
            data_transaksi[i]['item_id'], 
            data_transaksi[i]['jumlah_item'], 
            data_transaksi[i]['harga_item'], 
            data_transaksi[i]['total_harga'], 
            data_transaksi[i]['status']];
        ws_data.push(model);
    }

    var wb = XLSX.utils.book_new();

    var ws = XLSX.utils.aoa_to_sheet(ws_data);

    ws['!merges'] = [{ s: 'A1', e: 'F1' }];

    ws['!cols'] = wscols;

    //ws['!rows'] = wsrows;

    //ws['!autofilter'] = { ref: "A4:E4" };

    XLSX.utils.book_append_sheet(wb, ws, ws_name);

    var wopts = { bookType:'xlsx', bookSST:false, type:'binary' };

    var wbout = XLSX.write(wb,wopts);

    var filename = "Transaksi.xlsx";

    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), filename);
}

function generateJson(){
    saveAs(new Blob([JSON.stringify(data_transaksi)],{type:"application/json"}), "transaksi.json");
}
