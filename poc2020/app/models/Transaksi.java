package models;

import java.util.List;
import java.util.Date;

import models.enums.StatusOfTransaksi;
//import play.db.jdbc.BaseTable;
//import play.db.jdbc.CacheMerdeka;
//import play.db.jdbc.Id;
//import play.db.jdbc.Table;

import play.db.jpa.*;
import javax.persistence.*;

@Entity
@Table(name="transaksi")
public class Transaksi extends Model {

	//@Id(function = "nextval", sequence = "seq_transaksi")
	//public Long id;
	public Long user_id;
	public Long merchant_id;
    public Long item_id;
    public Integer jumlah_item;
    public Float harga_item;
    public Float total_harga;
    public Date tanggal;
    public String alamat;
    
    public StatusOfTransaksi status;
    public String pengiriman;
	
	public static List<Transaksi> getAll()
	{
		return find("ORDER BY tanggal desc").fetch();
	}

	public static List<Transaksi> getByUserId(Long user_id)
	{
		return find("user_id", user_id).fetch();
	}

	public static List<Transaksi> getByMerchantId(Long user_id)
	{
		return find("merchant_id", user_id).fetch();
	}
	
	public User getUser(){
        if(user_id==null)
			return null;
		return User.findById(user_id);
	}
	
	public User getMerchant(){
        if(merchant_id==null)
			return null;
		return User.findById(merchant_id);
	}
	
	public Barang getBarang(){
        if(item_id==null)
			return null;
		return Barang.findById(item_id);
    }

}
