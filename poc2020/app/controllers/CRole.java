package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import com.google.gson.Gson;

import models.Role;

public class CRole extends BaseController {

    public static void index() {
        List<Role> roles = Role.getAll();
        String transJson2 = new Gson().toJson(roles);
        render(roles, transJson2);
    }

    public static void form(Long id){
        Role role = new Role();
        if(id != null) role = Role.findById(id);
        render(role);
    }

    public static void delete(Long id){
        Role role = Role.findById(id);
        role.delete();
        redirect("/CRole/index");
    }

    public void formSubmit(Role role){
        role.save();
        redirect("/CRole/index");
    }

}