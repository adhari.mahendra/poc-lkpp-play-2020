File-file *.jar dapat diakses via HTTP dengan URL:

https://gitlab.lkpp.go.id/eproc/local-repository/raw/master/[module]-[revision].jar

Project ini juga menyediakan repo untuk maven yang dipakai di spse3 dengan URL:

https://gitlab.lkpp.go.id/eproc/local-repository/raw/master/maven-repo

Play! => Pada file dependencies.yml, isikan sebagai berikut:

    - local-repository modules:
        type:       local
        artifact : https://gitlab.lkpp.go.id/eproc/local-repository/raw/master/[module]-[revision].jar